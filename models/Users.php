<?php

class Users_Model extends Models{

	function __construct() {
		parent::__construct();
	}

    public function checkLogin($username, $password)
    {
            $sql="SELECT * FROM users WHERE username=?";
			$params = array("s", $username);
            $results = $this->prepare_query($sql, $params, true);
			
			//$tmp = password_hash($password, PASSWORD_DEFAULT);
			
			//die(password_verify($password, $tmp)."\n".$password."\n".$results[0]['password']."\n".$tmp);
			
			return ($results && password_verify($password, $results[0]['password'])) ? $results : false;
            
            
            /*
            
            // Mysql_num_row is counting table row
            $count=mysqli_num_rows($result);
            // If result matched $myusername and $mypassword, table row must be 1 row

            if($count==1)
            {
                // Register $myusername, $mypassword and redirect to file "login_success.php"
                session_start();
                while ($row = mysqli_fetch_assoc($result))
                {
                    $_SESSION['myusername'] = $row['userName'];
                    $_SESSION['mypassword'] = $row['password'];
                    $_SESSION['myid'] = $row['id'];
                    $_SESSION['age'] = $row['age'];
                    $_SESSION['city'] = $row['city'];
                    $_SESSION['login'] = 'success';
                }
                echo "0";
                //header("location:login.php?user=".$_GET['user'].'&imgId='.$_GET["imgId"].'&rank='.$_GET["rank"]);
            }
            else
            {
                session_start();
                $_SESSION['login'] = 'error';
                echo "1";
            }              
            
            */
    }
	
	public function register($epassword, $usernamePro, $age, $country, $city)
	{
		
		$hashed_password = password_hash($epassword, PASSWORD_DEFAULT);
		
		$sql = "insert into users (userName, password, age, country, city)
		values (?,?,?,?,?);";
		
		$params = array("ssiss", $usernamePro,$hashed_password,$age,$country,$city);
		
		if($this->prepare_query($sql, $params))
			return $this->insert_id();
	}
	
	public function update($_id, $_username = false, $_password = false, $_country = false, $_city = false)
	{
		if (!$_password && !$_country && !$_city)
			return false;
			
		$sql = "UPDATE users SET";
		$params = array("");
		if ($_username)
		{
			$sql .=" `userName` = ?,";
			$params[0] .= "s";
			array_push($params, $_username);
		}
		if ($_password)
		{
			$sql .=" `password` = ?,";
			$params[0] .= "s";
			array_push($params, $_password);
		}
		if($_country)
		{
			$sql .= " `country` = ?,";
			$params[0] .= "s";
			array_push($params, $_country);
		}
		if($_city)
		{
			$sql .= " `city` = ?,";
			$params[0] .= "s";
			array_push($params, $_city);
		}

		//removes trailing , and continues query		
		$sql = substr($sql, 0, -1)." WHERE `id` = ?";
		$params[0] .= "i";
		array_push($params, $_id);
		//print_r($params);
		//die();
		return $this->prepare_query($sql, $params);	
	}
	
	public function getPosts($userId, $mapId = false, $limit = false, $offset = 0, $order = false)
	{
		$sql="SELECT photos.id, photos.userId, photos.title, photos.image, photos.rank, photos.lat, photos.lng, 
			UNIX_TIMESTAMP(photos.uploadDate) as uploadDate, UNIX_TIMESTAMP(photos.modified) as modified, maps.name as mapName
		FROM photos 
		JOIN maps ON maps.id = photos.mapId
		WHERE photos.userId = ?";

		$params = array("i", $userId);

		if ($mapId)
		{
			$sql .= " AND photos.mapId = ?";
			$params[0] .= 'i';
			array_push($params, $mapId);
		}

		if ($order == "rated")
		  $sql .= " ORDER BY photos.rank DESC";
		if ($order == "newest")
		  $sql .= " ORDER BY photos.uploadDate DESC";
		if ($order == "oldest")
		  $sql .= " ORDER BY photos.uploadDate ASC";

		if ($limit)
  		{
			$sql .= " LIMIT ?, ?";		
			$params[0] .= 'ii';
			array_push($params, $offset, $limit);
		}

		return $this->prepare_query($sql, $params, true);			
	}

	public function subscribe($userId, $mapId)
	{
		$sql = "INSERT INTO subscriptions (mapId, userId)
		VALUES (?, ?)
		ON DUPLICATE KEY UPDATE del = NOT del";

		$params = array("ii", $mapId, $userId);
		
		return $this->prepare_query($sql, $params);			
		
	}
	
	public function getSubscriptions($userId, $type = false, $limit = false, $offset = 0, $order = false)
	{
		$sql = "SELECT maps.id, maps.name, maps.image, UNIX_TIMESTAMP(maps.created) as created, 
		(SELECT COUNT(*) FROM subscriptions WHERE mapId = maps.id AND del = 0) as subCount FROM maps
		JOIN subscriptions ON subscriptions.userId = ? AND del = false
		WHERE maps.id = subscriptions.mapId";
		
		$params = array("i", $userId);

		if ($type)
		{
			$sql .= " AND maps.posts = ?";
			$params[0] .= 's';
			array_push($params, $type);
		}


		if ($order == "newest")
		  $sql .= " ORDER BY maps.created DESC";
		if ($order == "oldest")
		  $sql .= " ORDER BY maps.created ASC";
		  
		if ($limit)
  		{
			$sql .= " LIMIT ?, ?";		
			$params[0] .= 'ii';
			array_push($params, $offset, $limit);
		}
		
		return $this->prepare_query($sql, $params, true);			
		
	}
	
	
	public function getMaps($userId, $limit = false, $offset = 0, $order = false)
	{
		$sql = "SELECT DISTINCT maps.id, maps.name, maps.image, UNIX_TIMESTAMP(maps.created) as created, 
		(SELECT COUNT(*) FROM subscriptions WHERE mapId = maps.id AND del = 0) as subCount FROM maps
		JOIN subscriptions ON del = false
		WHERE maps.userId = ?";
		
		$params = array("i", $userId);

		if ($order == "newest")
		  $sql .= " ORDER BY maps.created DESC";
		if ($order == "oldest")
		  $sql .= " ORDER BY maps.created ASC";
		  

		if ($limit)
  		{
			$sql .= " LIMIT ?, ?";		
			$params[0] .= 'ii';
			array_push($params, $offset, $limit);
		}
		
		return $this->prepare_query($sql, $params, true);			
		
	}
	
	
	
	public function requestMap($email)
	{
		$sql = "INSERT INTO requestMap (email)
		VALUES (?)";

		$params = array("s", $email);
		
		return $this->prepare_query($sql, $params);			
		
	}
}
