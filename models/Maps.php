<?php

class Maps_Model extends Models{

	function __construct() {
		parent::__construct();
	}


    function getMapByName($mapName = false, $userId = false, $visible = true) {
    	$params = array();
    	//$params[0] = '';
    	
    	$sqlWhere = "";
		$andOr = " WHERE ";

		if (!$userId)
			$sql="SELECT maps.*, UNIX_TIMESTAMP(maps.created) as created, (SELECT COUNT(*) 
				FROM subscriptions 
				WHERE mapId = maps.id 
				AND del = 0) as subCount 
			FROM maps";
		else
		{
			$sql ="SELECT maps.*, UNIX_TIMESTAMP(maps.created) as created, (SELECT COUNT(*) 
				FROM subscriptions 
				WHERE mapId = maps.id 
				AND del = 0) as subCount, subscriptions.del as del, if(del=1,false,true) as subbed
			FROM maps 
			LEFT JOIN subscriptions 
			ON subscriptions.mapId = maps.id 
			AND subscriptions.userId = ?";
			$params[0] = "i";
			array_push($params, $userId);
		}
		if (!empty($mapName))	//have you specified a map name?
		{
			$sqlWhere .=$andOr."name = ?";
			$andOr = " AND ";
			
			isset($params[0]) ? $params[0] .= "s" : $params[0] = "s";
			array_push($params, $mapName);

		}
		else	//if not, you're looking for all maps attached to user
		{

			if ($visible)
			{
				$sqlWhere .= $andOr."maps.visibility >= 'public'";
			}
		
		}
		$map = $this->prepare_query($sql.$sqlWhere, $params, true);
		return $map;
		//

				
    }

    function getMapsById($id, $userId = false, $post=array('public'), $visible = true) {

		$ids = implode(",", array_fill(0, count($id), "?"));
		$andOr = " AND ";

		$sql="SELECT maps.*, UNIX_TIMESTAMP(maps.created) as created 
		FROM maps ";

		$sqlWhere = "WHERE maps.id in ($ids)";

		$len = count($id);
		$params[0] = str_repeat("i", $len);
		is_array($id) ? $params = array_merge($params, $id) : array_push($params, $id);
		


		if (is_array($id) && $visible)
		{
			$sqlWhere .=" AND maps.visibility >= 'public'";
			//$andOr = " OR ";
		}
		if (is_array($id) && !empty($post))
		{
			foreach ($post as $postType)
			{
				if($postType == "public")
				{				
					$sqlWhere .=$andOr."(posts = 'public' OR maps.userId = ?)";
					$andOr = " OR ";					
					$params[0] .= "i";
					array_push($params, $userId);
				}
				elseif($postType == "subscribers")
				{
					$sql .= "LEFT JOIN subscriptions as subs ON subs.userId = ? AND maps.id = subs.mapId AND subs.del != 1 ";
					$sqlWhere .= $andOr."(posts = 'subscribers' AND subs.userId = ?)";
					$andOr = " OR ";

					$params[0] .= "ii";
					array_splice($params, 1, 0, $userId);
					array_push($params, $userId);

				}
				elseif($postType == "moderated")
				{}
				/*elseif($postType == "owner")
				{
					 $sqlWhere = " AND (posts = 'owner' AND maps.userId = ?)";					
				}*/	
			}


//SELECT maps.*, UNIX_TIMESTAMP(maps.created) as created FROM maps LEFT JOIN subscriptions as subs ON subs.userId = ? AND maps.id = subs.mapId AND subs.del != 1 WHERE maps.id in (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) AND maps.visibility = 'public' AND (posts = 'public' OR maps.userId = ?) AND (posts = 'subscribers' AND subs.userId = ?)			
		}

//die($sql.$sqlWhere);
		$map = $this->prepare_query($sql.$sqlWhere, $params, true);
		return $map;
		//print_r($map);
    }

    function getMapIdByName($mapName) {
        $sql="SELECT id FROM maps WHERE name = '$mapName'";
        $map = $this->query($sql, true);
        return $map;
        //print_r($map);
    }

	function updateLocation($loc)
	{
		$loc = mysqli_real_escape_string($GLOBALS['db'], $loc);
		$sql = "INSERT INTO locations (loc, count) VALUES ('$loc', 1) ON DUPLICATE KEY UPDATE count = count+1";
		return $this->query($sql);
	}
}
