<?php

class Comments_Model extends Models{

	function __construct() {
		parent::__construct();
	}


	public function getComments($postId, $userId = false, $limit = false, $offset = false)
	{
		$params = array("i", $postId);

        $sql="SELECT comments.id, comments.postId, comments.parentId, comments.userId, comments.content, 
		UNIX_TIMESTAMP(comments.created) as created, UNIX_TIMESTAMP(comments.modified) as modified, users.userName
		FROM comments 
        JOIN users ON users.id = comments.userId
        WHERE comments.del != TRUE AND comments.postId = ?";

//      if ($userId)
//		{
//			$sql .= " AND comments.userId = ?";
//			$params[0] .= 'i';
//			array_push($params, userId);
//		}

        $sql .= " ORDER BY created DESC";
		  
		if ($limit)
  		{
			$sql .= " LIMIT ?, ?";		
			$params[0] .= 'ii';
			array_push($params, $offset, $limit);
		}

		return $this->prepare_query($sql, $params, true);
	}

	public function insertComment($postId, $userId, $text, $parent = NULL)
	{
		$params = array("iiis", $userId, $postId, $parent, $text);
		$sql = "INSERT into comments (userId, postId, parentId, content) VALUES (?, ?, ?, ?)";
        if ($this->prepare_query($sql, $params))
            return $this->insert_id();
	}
    
	public function updateComment($userId, $id, $text)
	{

		$params = array("sii", $text, $id, $userId);

		$sql = "UPDATE comments SET content = ? WHERE id=? AND userId = ?";
        
        if ($this->prepare_query($sql, $params) && $this->affected_rows())
            return $id;

	}    

	public function deleteComment($userId, $id)
	{

		$params = array("ii", $id, $userId);

		$sql = "UPDATE comments SET del = 1 WHERE id= ? AND userId = ?";
        
        if ($this->prepare_query($sql, $params) && $this->affected_rows())
            return true;

	}  
	
}
