<?php

class Posts_Model extends Models{

	function __construct() {
		parent::__construct();
	}

	public function getPost($id, $mapId = false, $userId = false, $liked = false)
	{
		$joinStr = "";
        if ($userId && $liked)
		{
			$params = array("ii", $userId, $id);
			$joinStr .= ", (select count(*) from likes where userId = ? and imageId = ?) as user";
		}
 		else
			$params = array("i", $id);
		
		$sql="SELECT photos.id, photos.userId, photos.mapId, photos.title, photos.text, photos.image, photos.rank, 
		photos.lat, photos.lng, UNIX_TIMESTAMP(photos.uploadDate) as uploadDate".$joinStr.",
		users.userName as userName, maps.name as mapName
		FROM photos 
		JOIN users ON users.id = photos.userId
		JOIN maps ON maps.id = photos.mapId		
        WHERE photos.id = ?";

		if ($mapId)
		{
			$ids = implode(",", array_fill(0, count($mapId), "?"));

			$sql .= " AND photos.mapId in ($ids)";

			foreach ($mapId as $id)
			{
				$params[0] .= "i";
				array_push($params, $id);
			}

		}


		return $this->prepare_query($sql, $params, true);			
		

	}

	public function getPosts($latN, $longN,$latS, $longS, $mapId, $order="rated", $limit = 20, $preview = false)
	{
		if ($preview)
			$sql="select photos.id, photos.mapId, photos.userId, photos.title, photos.image, photos.rank, photos.lat, photos.lng, 
			UNIX_TIMESTAMP(photos.uploadDate) as uploadDate, UNIX_TIMESTAMP(photos.modified) as modified, ";
		else
			$sql="select photos.id, photos.userId, photos.mapId, photos.title, photos.text, photos.image, photos.rank, 
		photos.lat, photos.lng, UNIX_TIMESTAMP(photos.uploadDate) as uploadDate, UNIX_TIMESTAMP(photos.modified) as modified, maps.name as mapName";


		$params = array("");

		foreach ($mapId as $id)
		{
			$params[0] .= "i";
			array_push($params, $id);
		}

		$params[0] .= "dddd";
		array_push($params, $latN, $latS, $longN, $longS);

		$ids = implode(",", array_fill(0, count($mapId), "?"));

		$sql.="users.userName 
		from photos 
		join users on photos.userId = users.id 
		where mapId in ($ids) and photos.lat <= ? and photos.lat >= ? 
		and photos.lng >= ? and photos.lng <= ? order by ";

		if ($order == "rated")
		  $sql .= "photos.rank desc";
		if ($order == "newest")
		  $sql .= "photos.uploadDate desc";
		if ($order == "oldest")
		  $sql .= "photos.uploadDate asc";

		if ($limit)
		{
			$sql .= " limit ?";
			$params[0] .= 'i';
			array_push($params, $limit);			
		}

		return $this->prepare_query($sql, $params, true);
		
	}

	public function insertPhoto($id, $mapId, $title, $text, $imgName, $lat, $lng)
	{
		$imgName = substr($imgName, 0, -4);

		$params = array('iisssdd', $id, $mapId, $title, $text, $imgName, $lat, $lng);
		
		$sql = "INSERT into photos (userId, mapId, title, text, image, lat, lng) VALUES (?, ?, ?, ?, ?, ?, ?)";
        if ($this->prepare_query($sql, $params))
            return $this->insert_id();
	}
    
	public function updatePost($userId, $id, /*$mapId=false, */$title=false, $text=false, $imgName=false)
	{

        $imgName ? substr($imgName, 0, -4) : $imgName = false;
        
		$params = array('');
        
		
		$sql = "UPDATE photos SET";
        
        //if ($mapId)
        //    $sql .= " mapId = $mapid";
        $sqlVars = '';
        if ($title)
        {
			$sqlVars .= !empty($sqlVars)?", title = ?":" title = ?";
			$params[0] .= 's';
			array_push($params, $title);
		}
        if ($text)
        {
			$sqlVars .= !empty($sqlVars)?", text = ?": " text = ?";
			$params[0] .= 's';
			array_push($params, $text);			
		}
        if ($imgName)
        {
			$sqlVars .= !empty($sqlVars)?", image = ?": " image = ?";
			$params[0] .= 's';
			array_push($params, substr($imgName, 0, -4));
		}
        
		$params[0] .= 'ii';
		array_push($params, $id, $userId);
        $sql .= $sqlVars." WHERE id= ? AND userId = ?";
        
        if ($this->prepare_query($sql, $params) && $this->affected_rows())
            return $id;

	}    

	public function deletePost($userId, $id)
	{

		$params = array('ii', $id, $userId);
//die(print_r($params, true));
		$sql = "DELETE FROM photos WHERE id= ? AND userId = ?";
        
        if ($this->prepare_query($sql, $params) && $this->affected_rows())
            return true;

	}  
	
}
