<?php
	if (empty($uri))
		return false;

	require_once BASE_DIR."Controllers.php";
	require_once BASE_DIR."Models.php";
	require_once BASE_DIR."Views.php";

	$cont = null;

	//override controller and function names
	switch ($uri[0]):
		case('m'):
			/*$uri[0] = "maps";
			$uri[2] = $uri[1];*/
			array_unshift($uri, "maps");
			$uri[1] = "index";
		break;
		case('a'):
			array_unshift($uri, "maps");
			$uri[1] = "atlas";
		break;		
		case('login'):
			$uri[0] = "users";
			$uri[1] = "login";
		break;
		case('about'):case('terms'):case('privacy'):
			$uri[1] = "page";
			$uri[2] = $uri[0];
			$uri[0] = "home";
		break;		
	endswitch;

	//print_r($uri);

	//support for legacy code.  goal is to REMOVE this line
	if ($uri[0] == "functions" || $uri[0] == "search"|| $uri[0] == "upload" || $uri[0] == "uploadfunc")
		file_exists(DOC_ROOT.$uri[0].".php") ? require DOC_ROOT.$uri[0].".php" : (new Views)->statusPage(404);
	elseif($uri[0] === "scripts" || $uri[0] === "css")
		return;
	else
	{
		
		file_exists(DOC_ROOT."controllers/".$uri[0].".php") ? require (DOC_ROOT."controllers/".$uri[0].".php") : (new Views)->statusPage(404);
		
		$class = ucfirst($uri[0]);
		$controller = new $class($uri);

		if (empty($uri[1]))
			$controller->index();
		else
		{
			$function = $uri[1];
			isset($uri[2])?$properties = $uri[2]:$properties = false;
				//print_r($controller);
			if (method_exists($controller, $function) )
				$controller->$function($properties);
			else
				(new Views)->statusPage(404);
		}
	}