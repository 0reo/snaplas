<?php

	define('AUTH_SALT', uniqid('snp'));

	//internal defines
	define("DEBUG_MODE", true);
	define("DOC_ROOT", $_SERVER['DOCUMENT_ROOT']."/");
	define("CONFIG_DIR", $_SERVER['DOCUMENT_ROOT']."/config/");
	define("BASE_DIR", $_SERVER['DOCUMENT_ROOT']."/base/");
	define("CONTROLLERS_DIR", $_SERVER['DOCUMENT_ROOT']."/controllers/");
	define("VIEWS_DIR", $_SERVER['DOCUMENT_ROOT']."/views/");
	define("TEMPLATES_DIR", VIEWS_DIR."templates/");
	define("MODELS_DIR", $_SERVER['DOCUMENT_ROOT']."/models/");
	define("HELPERS_DIR", $_SERVER['DOCUMENT_ROOT']."/helpers/");	
	define("LIBRARIES_DIR", $_SERVER['DOCUMENT_ROOT']."/libraries/");	
	define("STATUS_DIR", $_SERVER['DOCUMENT_ROOT']."/status_pages/");

	//external defines
	define("JS_DIR", "/scripts/");
	define("CSS_DIR", "/css/");
