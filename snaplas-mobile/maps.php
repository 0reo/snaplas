<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=320, initial-scale=1">
<title>Snaplas Mobile</title>
<link href="main.css" rel="stylesheet" type="text/css">
<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,800' rel='stylesheet' type='text/css'>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script> 
<script type="text/javascript">
	var maplisttemplate = '<? include ("map-list-item.php") ?>';
</script>
</head>

<body>
<main id="main-box">
	<?=$nav ?>
    <div id="map-list-container">
    	
    </div>
</main>
<script src="main.js"></script>
</body>
</html>