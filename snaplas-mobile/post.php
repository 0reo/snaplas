<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=320, initial-scale=1">
<title>Snaplas Mobile</title>
<link href="main.css" rel="stylesheet" type="text/css">
<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,800' rel='stylesheet' type='text/css'>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script> 
</head>

<body>
<main id="main-box">
	<nav id="nav">
    	<a class="nav-btn">Maps</a>
        <a class="nav-btn">Posts</a>
        <a class="nav-btn">Log Out</a>
    </nav>
  	<form id="post-a-post-form-size" action="Post a Post" method="post"> 
   		<div id="post-a-post-container">
			<input class="post-a-post-title" name="Post Title" type="text" value="Title...">
       	  	<div id="post-a-post-image-upload">Upload an Image</div>
            <textarea id="post-a-post-description" name="Description" cols="" rows="3">Description (optional)</textarea>
        
   		</div>
   		<div id="post-a-post-btn">Continue</div> 
   	</form>
</main>
<script src="main.js"></script>
</body>
</html>