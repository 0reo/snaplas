<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=320, initial-scale=1">
<title>Snaplas Mobile</title>
<link href="main.css" rel="stylesheet" type="text/css">
<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,800' rel='stylesheet' type='text/css'>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script> 
</head>

<body>
<main id="main-box">
	<nav id="nav">
    	<a class="nav-btn">Maps</a>
        <a class="nav-btn">Posts</a>
        <a class="nav-btn">Log Out</a>
    </nav>
    <div id="post-list-container">
    	<a class="post-item-box">
        	<div class="post-image"></div>
            <div class="post-info-box">
            	<div class="post-info-name">
                	Name of Post
                </div>
                <time class="post-info-date" datetime="#">15 July 2014 8:56PM</time>
            </div>
            <div class="post-edit-icon"></div>
        </a>
       <a class="post-item-box"></a>
       <a class="post-item-box"></a>
       <a class="post-item-box"></a>
       <a class="post-item-box"></a>
       <a class="post-item-box"></a>
    </div>
    <div id="post-new">New Post</div>
</main>
<script src="main.js"></script>
</body>
</html>