<!DOCTYPE html>
	<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="viewport" content="width=320, initial-scale=1">
		<title>Snaplas Mobile</title>
		<link href="main.css" rel="stylesheet" type="text/css">
		<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,800' rel='stylesheet' type='text/css'>
		<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script> 
	</head>

	<body>
		<main id="main-box">
			<div id="login-logo-box">
				<div id="login-logo-mobile">mobile</div>
			</div>
			<div id="login-form-container">
				<form action="Login" method="post" name="Login">
					<input class="login-form-box" type="text" value="username">
					<input class="login-form-box" type="password" value="password">
					<input id="login-sign-in-btn" name="Sign In" type="submit" value="Sign In">
				 </form>
			</div>
			<div id="login-use-description">
				Upload and manage posts with your mobile device.
			</div>
		</main>
	</body>
</html>