<?php
	session_start();
	session_unset();
	session_destroy();
	session_write_close();
	setcookie(session_name(),'',0,'/');
?>
<!DOCTYPE html>
<html>
<head>
	<script type="text/javascript"> 
		self.parent.location.href = "./";
	</script>
</head>
</html>
