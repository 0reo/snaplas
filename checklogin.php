<?php
	require_once CONFIG_DIR.'database.php';
	$tbl_name="users"; // Table name

	// username and password sent from form
	$myusername=$_POST['username'];
	$mypassword=$_POST['password'];

	// To protect MySQL injection (more detail about MySQL injection)
	$myusername = stripslashes($myusername);
	$mypassword = stripslashes($mypassword);
	$myusername = mysqli_real_escape_string($db, $myusername);
	$mypassword = mysqli_real_escape_string($db, $mypassword);

	$mypassword = crypt($mypassword, '$6$rounds=5000$'.AUTH_SALT.'$');
	$sql="SELECT * FROM $tbl_name WHERE username='$myusername' and password='$mypassword'";
	$result=mysqli_query($db, $sql);

	// Mysql_num_row is counting table row
	$count=mysqli_num_rows($result);
	// If result matched $myusername and $mypassword, table row must be 1 row

	if($count==1)
	{
		// Register $myusername, $mypassword and redirect to file "login_success.php"
		session_start();
		while ($row = mysqli_fetch_assoc($result))
		{
			$_SESSION['myusername'] = $row['userName'];
			$_SESSION['mypassword'] = $row['password'];
			$_SESSION['myid'] = $row['id'];
			$_SESSION['age'] = $row['age'];
			$_SESSION['city'] = $row['city'];
			$_SESSION['login'] = 'success';
		}
		echo "0";
		//header("location:login.php?user=".$_GET['user'].'&imgId='.$_GET["imgId"].'&rank='.$_GET["rank"]);
	}
	else
	{
		session_start();
		$_SESSION['login'] = 'error';
		echo "1";
	}
?>
