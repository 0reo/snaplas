<?php
ini_set("session.cookie_lifetime","86400"); //2 hours

	
class Controllers {

	protected $data = null;

	function __construct($_data) {
		$this->data = $_data;
		$this->models = array();
		$this->libraries = array();
		$this->views = new Views();
		$this->loadHelper("input");
		$this->loadHelper("variable");
		$this->loadHelper("session");
		$this->loadLibrary("Mobile_Detect","mobiledetect/mobiledetectlib/Mobile_Detect");		
	}
	
	function index()
	{
		return true;
	}

	function loadModel($_modelName)
	{

		if (!file_exists(MODELS_DIR.$_modelName.'.php'))
			return false;
		else
		{
			require_once(MODELS_DIR.$_modelName.".php");
			$_modelClassName = $_modelName.'_Model';
			$this->models[$_modelName] = new $_modelClassName();
		}
	}

	function loadHelper($_helperName)
	{

		if (!file_exists(HELPERS_DIR.$_helperName.'Helper.php'))
			return false;
		else
		{
			require_once(HELPERS_DIR.$_helperName."Helper.php");
		}
	}
	
	function loadLibrary($_libName, $_libPath = false, $_properties = array())
	{

		if (!$_libPath)
			$_libPath = $_libName;

		if (!file_exists(LIBRARIES_DIR.$_libName.'.php'))
			return false;
		else
		{
			require_once LIBRARIES_DIR.'autoload.php';
			require_once(LIBRARIES_DIR.$_libName.".php");
			$this->libraries[$_libName] = new $_libName($_properties);
		}
	}
	
	function checkSession()
	{
		if (session_status() == PHP_SESSION_NONE)
			session_start();
		return isset($_SESSION['login']) && $_SESSION['login'] === 'success';
	}

}
