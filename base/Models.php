<?php

require_once CONFIG_DIR.'database.php';

class Models {

	function __construct() {
	}

	function query($_sql, $xss = false)
	{
		$result = mysqli_query($GLOBALS['db'], $_sql);
		$res = array();
		if ($result)
		{
			if (!is_bool($result))
			{
				while ($row = mysqli_fetch_assoc($result)) {
					array_push($res, $row);
				}
				if ($xss)
					array_walk_recursive($res, function(&$val){
						$val = filter_var($val, FILTER_SANITIZE_STRING, array("flags"=>FILTER_FLAG_NO_ENCODE_QUOTES));
					});
				return $res;
			}
			else
				return $result;
		}

		if (!$GLOBALS['errors']->setError("mysql", mysqli_error($GLOBALS['db'])."\nQuery - ".$_sql, mysqli_errno($GLOBALS['db'])))
			die ("could not set error: ".mysqli_error($GLOBALS['db'])."\nQuery - ".$_sql);
		
		/*if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest')
		{
			$return = array("status"=>"err", "err"=>"", "msg"=>print_r($errors->viewErrors("mysql")), "data"=>"\n");
			die(print_r($return, true));
		}
		else*/
		//	echo mysqli_error($GLOBALS['db'])."\nQuery - ".$_sql."\n";
		$GLOBALS['errors']->viewErrors("mysql");
		return false;
	}
	
	
	function prepare_query($_sql, $_params = array(), $xss = false)
	{
//die($_sql);

		if ($GLOBALS['stmt'] = mysqli_prepare($GLOBALS['db'], $_sql)) {

			/* bind parameters for markers */
			//$_params = $this->makeValuesReferenced($_params);

			if (!empty($_params))
				call_user_func_array('mysqli_stmt_bind_param', $this->makeValuesReferenced(array_merge(array($GLOBALS['stmt']), $_params))); 

			//$stmt_bind = new ReflectionFunction('mysqli_stmt_bind_param');
			//$stmt_bind->invokeArgs($_params);


			/* execute query */
		    if (mysqli_stmt_execute($GLOBALS['stmt']))
			{

				$result = mysqli_stmt_get_result($GLOBALS['stmt']);
				$res = array();

				/* fetch value */
				if ($result)
				{
					while ($row = mysqli_fetch_assoc($result)) {
						array_push($res, $row);
					}
					if ($xss)
						array_walk_recursive($res, function(&$val){
							$val = filter_var($val, FILTER_SANITIZE_STRING, array("flags"=>FILTER_FLAG_NO_ENCODE_QUOTES));
						});
					return $res;
				}
				return true;
			}
			if (!$GLOBALS['errors']->setError("mysql", "Prepared no result - ".mysqli_stmt_error($GLOBALS['stmt'])."\nQuery - ".$_sql."\nParameters - ".print_r($_params, true), mysqli_stmt_errno($GLOBALS['stmt'])))
				die ("could not set error: ".mysqli_stmt_error($GLOBALS['stmt'])."\nQuery - ".$_sql);
				
			/* close statement */
			mysqli_stmt_close($GLOBALS['stmt']);
		}	
		else
			if (!$GLOBALS['errors']->setError("mysql", "Failed to prepare statement - \nQuery - ".$_sql."\nParameters - ".print_r($_params, true), mysqli_stmt_errno($GLOBALS['stmt'])))
				die ("could not set error: Failed to prepare statement - \nQuery - ".$_sql);			


		$GLOBALS['errors']->viewErrors("mysql");
		return false;

	}

	function escapeString($string, $xss = true)
	{
		//if ($xss)
		//	$string = htmlspecialchars($string,  ENT_COMPAT | ENT_HTML401, 'UTF-8');
		return mysqli_real_escape_string($GLOBALS['db'], $string);
	}
	
	function insert_id()
	{
		return mysqli_insert_id($GLOBALS['db']);	
	}
	
	function affected_rows($_prod = false)
	{
		if ($_prod)
			return  mysqli_stmt_affected_rows($GLOBALS['stmt']);
		return mysqli_affected_rows($GLOBALS['db']);
	}
	
	
	
    private function makeValuesReferenced($arr){ 
        $refs = array(); 
        foreach($arr as $key => $value) 
        $refs[$key] = &$arr[$key]; 
        return $refs; 

    } 	
}