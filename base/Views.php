<?php

class Views {

	
	function __construct($_data = array()) {
		$this->data = $_data;
	}
	
	function statusPage($status = 404)
	{
		require_once(STATUS_DIR.$status.".php");
		die();
	}

	function loadView($_viewName, $_data=array(), $output = false)
	{
		$GLOBALS['errors']->viewErrors("mysql");
		
		if (file_exists(VIEWS_DIR.$_viewName.".php"))
		{
			if (is_array($_data))
				foreach ($_data as $key => $value)
					$$key = $value;
			
			if ($output) ob_start();
			include (VIEWS_DIR.$_viewName.".php");
			if ($output) return ob_get_clean();

		}
		/*else
		{
			require_once(STATUS_DIR."404.php");
		}*/
	}

	function loadTemplate($_templateName, $_type="js", $_data=array(), $output = false)
	{
		if (file_exists(TEMPLATES_DIR.$_templateName.".php"))
		{
			if (is_array($_data))
				foreach ($_data as $key => $value)
					$$key = $value;
			
			if ($_type == "js")
			{
				if ($output) ob_start();
					echo '<script id="'.$_templateName.'" type="x-tmpl-mustache">';
					include (TEMPLATES_DIR.$_templateName.".php");
					echo '</script>';
				if ($output) return ob_get_clean();				
			}
			elseif($_type == "php")
			{			
				$m = new Mustache_Engine;
				if ($output)
					return $m->render(file_get_contents(TEMPLATES_DIR.$_templateName.".php"), $_data); // "Hello, World!"
				else
					echo $m->render(file_get_contents(TEMPLATES_DIR.$_templateName.".php"), $_data); // "Hello, World!"
			}

		}		
	}

}
