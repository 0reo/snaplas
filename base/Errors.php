<?php

class Errors
{
    protected $errors;
    function __construct()
    {
        $this->errors = array(
            "mysql" =>array()
        );
    }

    //takes a predefined error type and the message.
    //if the error type is valid, add it to the error stack
    public function setError($type, $val, $errno = -1)
    {
        if (isset($this->errors[$type]))
        {
            array_push($this->errors[$type], array("msg"=>$val, "errno"=>$errno, 'viewed'=>false));
            return true;
        }
        return false;
    }


    public function getErrors($type = false)
    {
        if (!$type)
        {
            return $this->errors;
        }
        elseif (!empty($this->errors[$type]))
        {
            return $this->errors[$type];
        }
    }
    
    public function viewErrors($type = false)
    {
        
        if (!$type)
        {
            foreach ($this->errors as $errorType=>$errors)
            {
                if (empty($errors))
                    continue;
                if (DEBUG_MODE)
                    echo $errorType." errors - \n";
                error_log($errorType." errors - ");
                foreach ($errors as $error)
                {
                    if (!$error['viewed'])
                    {
                        if (DEBUG_MODE)
                            echo $error['errno']." - ".$error['msg']."\n";
                        $error['viewed'] = true;
                        error_log($error['errno']." - ".$error['msg']);
                    }
                }
                if (DEBUG_MODE)
                    echo "\n";
            }
        }
        elseif (!empty($this->errors[$type]))
        {
            if (DEBUG_MODE)
                echo $type." errors - \n";
            error_log($type." errors - ");
            foreach ($this->errors[$type] as $error)
            {
                if (!$error['viewed'])
                {
                    if (DEBUG_MODE)
                        echo $error['errno']." - ".$error['msg']."\n";
                    $error['viewed'] = true;
                    error_log($error['errno']." - ".$error['msg']);
                }
            }
            if (DEBUG_MODE)
                echo "\n";
        }
    }
}
