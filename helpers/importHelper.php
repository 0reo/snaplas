<?php

	function loadCSS($styles = array())
	{
		$out = '';
		foreach($styles as $style)
		{
			if (strpos($style, 'http://') !== false || strpos($style, 'https://') !== false || strpos($style, '//') !== false)
				$out .='<link rel="stylesheet" type="text/css" href="'.$style.'">'."\n";
			else
				$out .='<link rel="stylesheet" type="text/css" href="'.CSS_DIR.$style.'.css">'."\n";

		}
		return $out;
	}

	function loadJS($scripts = array())
	{
		$out = '';
		foreach($scripts as $script)
		{
			if (strpos($script, 'http://') !== false || strpos($script, 'https://') !== false || strpos($script, '//') !== false)
				$out .='<script type="text/javascript" src="'.$script.'"></script>'."\n";
			else
				$out .='<script type="text/javascript" src="'.JS_DIR.$script.'.js"></script>'."\n";
		}
		return $out;
	}