<?php

    function getPost($post = false)
    {
        if (!$post)
            return $_POST;
        elseif (!empty($_POST[$post]))
            return $_POST[$post];
        else
            return false;
    }
    
    function getGet($get = false)
    {
        if(!$get)
            return $_GET;
        elseif (!empty($_GET[$get]))
            return $_GET[$get];
        else
            return false;
    }
    
    function getFile($name, $single = true)
    {
        $FILES = reArrayFiles($_FILES[$name]);
        if(!$single)
            return $FILES;
        elseif ($single && !empty($FILES[$name]))
            return $FILES[$name];
        else
            return false;        
    }

    function reArrayFiles(&$file_post) {
        // cause php is stupid.  thanks phpuser
        // http://php.net/manual/en/features.file-upload.multiple.php#53240

        $file_ary = array();
        $file_count = count($file_post['name']);
        $file_keys = array_keys($file_post);

        for ($i=0; $i<$file_count; $i++) {
            foreach ($file_keys as $key) {
                $file_ary[$i][$key] = $file_post[$key][$i];
            }
        }

        return $file_ary;
    }