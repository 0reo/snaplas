<?php
    function convertImage($_file, $_srcDir, $_destDir, $_type = "jpg", $quality = null)
    {


        // Target dimensions
        $max_width =1920;
        $max_height = 1080;

        $path_parts = pathinfo($_srcDir.$_file);	//get file info
        $ext = $path_parts['extension'];

/*            // jpg, png, gif or bmp?
        $exploded = explode('.',$_srcDir.$_file);
        $ext = $exploded[count($exploded) - 1]; */

        if (preg_match('/jpg|jpeg/i',$ext))
        {
            $imageTmp=imagecreatefromjpeg($_srcDir.$_file);
            $exif = exif_read_data($_srcDir.$_file);
        }
        else if (preg_match('/png/i',$ext))
            $imageTmp=imagecreatefrompng($_srcDir.$_file);
        else if (preg_match('/gif/i',$ext))
            $imageTmp=imagecreatefromgif($_srcDir.$_file);
        else if (preg_match('/bmp/i',$ext))
            $imageTmp=imagecreatefrombmp($_srcDir.$_file);
        else
            return 0;


        $old_width  = imagesx($imageTmp);
        $old_height = imagesy($imageTmp);

        // Calculate the scaling we need to do to fit the image inside our frame
        $scale = min($max_width/$old_width, $max_height/$old_height);

        // Get the new dimensions
        $new_width  = ceil($scale*$old_width);
        $new_height = ceil($scale*$old_height);

        $imageScaled = imagecreatetruecolor($new_width, $new_height);//make thumbnail image




        if (!empty($exif['Orientation'])) {
            switch ($exif['Orientation']) {
                case 3:
                $imageTmp = imagerotate($imageTmp, 180, 0);
                break;

                case 6:
                $imageTmp = imagerotate($imageTmp, -90, 0);
                break;

                case 8:
                $imageTmp = imagerotate($imageTmp, 90, 0);
                break;
            }
        }

        imagecopyresampled($imageScaled, $imageTmp, 0, 0, 0, 0, $new_width, $new_height, $old_width, $old_height);//make thumbnail


        // quality is a value from 0 (worst) to 100 (best)
        if ($_type === "jpg")
            imagejpeg($imageScaled, $_destDir.$path_parts['filename'].".jpg", $quality);
        elseif ($_type === "png")
            imagepng($imageScaled, $_destDir.$path_parts['filename'].".png", $quality);
        elseif ($_type === "gif")
            imagepng($imageScaled, $_destDir.$path_parts['filename'].".gif");                
        imagedestroy($imageTmp);
        return 1;
    }     


    function makethumb($_file, $_srcDir, $_destDir, $_width = 160, $_height = 100)
    {

            $path_parts = pathinfo($_srcDir.$_file);	//get file info
            $ext = $path_parts['extension'];

            list($width, $height) = getimagesize($_srcDir.$_file);//get image size

            $imageThumb = imagecreatetruecolor(160, 100);//make thumbnail image

            if (preg_match('/jpg|jpeg/i',$ext))
            {
                $imageTmp = imagecreatefromjpeg($_srcDir.$_file);	
                $exif = exif_read_data($_srcDir.$_file);
            }
            elseif (preg_match('/png/i',$ext))
                $imageTmp = imagecreatefrompng($_srcDir.$_file);
            elseif (preg_match('/gif/i',$ext))
                $imageTmp = imagecreatefrompng($_srcDir.$_file);
            elseif (preg_match('/bmp/i',$ext))
                $imageTmp = imagecreatefrompng($_srcDir.$_file);
            else
                return 0;                    

            if (!empty($exif['Orientation'])) {
                switch ($exif['Orientation']) {
                    case 3:
                    $imageTmp = imagerotate($imageTmp, 180, 0);
                    break;

                    case 6:
                    $imageTmp = imagerotate($imageTmp, -90, 0);
                    break;

                    case 8:
                    $imageTmp = imagerotate($imageTmp, 90, 0);
                    break;
                }
            }

            imagecopyresampled($imageThumb, $imageTmp, 0, 0, 0, 0, $_width, $_height, $width, $height);//make thumbnail

            imagejpeg($imageThumb, $_destDir.$path_parts['filename'].".jpg", 100);		// Output



    }

