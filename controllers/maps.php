<?php
	require_once DOC_ROOT."functions.php";
	require_once DOC_ROOT."classes.php";


	class Maps extends Controllers
	{

		function __construct($_data) {
			parent::__construct($_data);
			$this->loadModel("Maps");
			$this->loadModel("Users");
			$this->loadModel("Posts");
			$this->loadModel("Comments");
			$this->loadHelper("import");
		}

		public function index()
		{
			$session = $this->checkSession();
			
			if($this->libraries['Mobile_Detect']->isMobile() && !$this->libraries['Mobile_Detect']->isTablet())
			{
				header("location:/mobileupload");
			}
			
			//make sure map exsists
			//data[0] is the controler, 1 is the function, 2 is the map name, 3 is the post
			if ($this->data[0] !== "maps" || empty($this->data[1]))
				return false;
			else
			{
				$data['mapName'] = str_replace('-', ' ', $this->data[2]);
				$map = $this->models['Maps']->getMapByName($data['mapName'], getUserId());
				if ($map)
				{
					$map = $map[0];
					//die(print_r($map, true));
					if (isset($map['del']))
						$map['del'] !== ""? : $map['del']=1;//makes sure 
					$map['lgd'] = getUserId();
					$data['mapId'] = $map['id'];
					$data['mapDesc'] = htmlentities($map['description']);

					isset($map['image'])?$data['image'] = $map['image']:$map['image']=false;
					$data['startZoom'] = $map['startZoom'];
					$data['startLat'] = $map['startLat'];
					$data['startLng'] = $map['startLng'];
					$data['myMap'] = $map['userId'] == getUserId();


					//do we display the post to map icon?
					if ($data['myMap'] ||
					$map['posts'] == "public" ||
					($map['posts'] == "subscribers" && $map['subbed']) )
						$data['canPost'] = true;
					else
						$data['canPost'] = false;


					//die(print_r($data['canPost'], true));


					/*$mapInfo = array("lgd"=>getUserId(), 
					"image"=>!empty($data['image'])?$data['image']:'', 
					"mapId"=>$data['mapId'], 
					"name"=>$data['mapName'], 
					"created"=>$data['created'], 
					"sub"=>$data['sub'], 
					"desc"=>$data['mapDesc']);*/
				}
				else
					exit();

			}
			$data['mapbar'] = $this->views->loadView("blocks/mapbar", array(), true);
			$data['popups'] = "";


			///wtf dues this do??
			if (isset($_GET['loc']))
				$this->models['Maps']->updateLocation($_GET['loc']);

			$post = array();
			$comments = array();
			
			if (isset($_GET['photoId']) || isset($this->data[3]))
			{
				isset($_GET['photoId']) ? $photoId = $_GET['photoId'] : $photoId = $this->data[3];
				
				$post = $this->models['Posts']->getPost($photoId);
				
				if (!empty($post))
				{
					 $post = $post[0];
					if($post['id']):
						

						$data['pId'] = $post['id'];
						$data['lat'] = $post['lat'];
						$data['lng'] = $post['lng'];
						$data['locate'] = "false";
						$data['author'] = $post['userName'];
						
						!empty($post['title'])?$data['postName'] = $post['title']." | ":false;
						!empty($post['text'])?$data['mapDesc'] = htmlentities($post['text']):false;
						isset($post['image'])?$data['image'] = $post['image'].".jpg":false;

						$post['datetime'] = gmdate('Y-m-d\TH:i:s\Z', $post['uploadDate']);
						$post['uploadDate'] = gmdate('D, M j g:i a', $post['uploadDate']);
						$post['mapUrl'] = str_replace(' ', '-', $post['mapName']);

					endif;

					$comments = $this->models['Comments']->getComments($photoId);				 
				}
				else
				{
					header('Location: /m/'.$data['mapName']);
				}
			}
			else
				$data['locate'] = "true";

			$data['postTemplate'] = $this->views->loadTemplate("post", 'php', $post, true);
			$data['comments'] = $this->views->loadView("blocks/comments", array("comments"=>$comments), true);


			$this->generateMap($session, $data, $map);
		}

		public function atlas()
		{
			$session = $this->checkSession();

			if($this->libraries['Mobile_Detect']->isMobile() && !$this->libraries['Mobile_Detect']->isTablet())
			{
				header("location:/mobileupload");
			}
			
			//make sure maps exsists
			//data[0] is the controler, 1 is the function, 2 is the map name, 3 is the post
			if ($this->data[0] !== "maps" || empty($this->data[1]))
				return false;
			else
			{
				$data['mapName'] = str_replace('-', ' ', $this->data[2]);
				if ($data['mapName'] == "All")
					$maps = $this->models['Maps']->getMapByName("", getUserId());
				else
					$maps = $this->models['Maps']->getMapByName($data['mapName'], getUserId());
				if ($maps)
				{
					$data['mapDesc'] = htmlentities($maps[0]['description']);
					isset($maps[0]['image'])?$data['image'] = $maps[0]['image']:$maps[0]['image']=false;
					$data['startZoom'] = 3;
					$data['startLat'] = 0;
					$data['startLng'] = 0;
					$data['canPost'] = array();
					$data['myMap'] = array();
					$data['mapId'] = array();
															
					foreach ($maps as $map)
					{
						$map['lgd'] = getUserId();
						array_push($data['myMap'], $map['userId'] == getUserId());
						array_push($data['mapId'], $map['id']);

						//do we display the post to map icon?
						$canPost = false;
						if ($data['myMap'] ||
						$map['posts'] == "public" ||
						($map[0]['posts'] == "subscribers" && $map[0]['subbed']) )
							$canPost = true;

						array_push($data['canPost'], $canPost);

						/*$mapInfo = array("lgd"=>getUserId(), 
						"image"=>!empty($data['image'])?$data['image']:'', 
						"mapId"=>$data['mapId'], 
						"name"=>$data['mapName'], 
						"created"=>$data['created'], 
						"sub"=>$data['sub'], 
						"desc"=>$data['mapDesc']);*/
					}
					$data['mapDesc'] = "Snaplas is an atlas to discover pictures, stories and moments";					
					$maps['name'] = $this->data[2];
					$maps['image'] = "all-map.jpg";
					$maps['created'] = "0";
					$maps['description'] = "Welcome to All. Here you can explore All the public content on Snaplas. When you're posting from All, please make sure to choose the correct map to post to.";
					
				}
				else
					exit();
				$data['mapId'] = implode($data['mapId'], ",");
			}
			$data['mapbar'] = $this->views->loadView("blocks/mapbar", array(), true);
			$data['popups'] = "";



			$data['locate'] = "true";
			$data['postTemplate'] = $this->views->loadTemplate("post", 'php', array(), true);
			$data['comments'] = $this->views->loadView("blocks/comments", array("comments"=>array()), true);


			
			$this->generateMap($session, $data, $maps);
		}

		private function generateMap($session, $data, $_mapInfo)
		{
			if(!$session)
			{
				$data['loginBtn'] = '<div id="login" class="tooltip" data-tip="Sign in/register">Sign In</div>'."\n";
				$data['popups'] = '
				<div class="popup" id="newProfile"></div>';
				$data['managementBar'] = "";
				$data['postManager'] = "";
				$data['subscriptionsManager'] = "";				
			}
			else
			{
				$data['loginBtn'] = '<div id="profileBtn" class="tooltip" data-tip="View/change profile settings">Profile</div>'."\n";
				$data['popups'] = '
				<div class="popup" id="share"></div>
				<aside class="popup" id="profile"></aside>
				<div class="popup" id="upload"></div>';

				$data['managementBar'] = $this->views->loadView("blocks/managementBar", array(), true);
				$data['postManager'] = $this->views->loadView("blocks/managePosts", array(), true);				
				$data['subscriptionsManager'] = $this->views->loadView("blocks/manageSubscriptions", array(), true);				
				$data['mapsManager'] = $this->views->loadView("blocks/manageMaps", array(), true);				
				
			}

			$data['css'] = array('reset', 'main','leaflet', 'LeafletStyleSheet', 'snaplas-theme/jquery-ui-1.10.4.custom.min','//fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,800','perfect-scrollbar.min');

			if (file_exists(DOC_ROOT.'scripts/main.min.js'))
				$data['js'] = array('leaflet', 'PruneCluster', 'jquery-1.9.0.min', 'jquery.hoverIntent', 'jquery-ui-1.10.4.custom.min', 'mustache', 'moment.min', 'main.min', '//platform.twitter.com/widgets.js', 'perfect-scrollbar.with-mousewheel.min', 'prefixfree.min');

			else
				$data['js'] = array('leaflet-src', 'PruneCluster', 'jquery-1.9.0', 'jquery.hoverIntent', 'jQuery.XDomainRequest', 'jquery-ui-1.10.4.custom.min', 'mustache', '//platform.twitter.com/widgets.js', 'perfect-scrollbar.with-mousewheel.min', 'moment.min', 'snapmap','profile','posts','main','search','contact','uploadFile');

			$data['popups'] .= $this->views->loadView("blocks/popupsDefault", array(), true);
			$data['mapInfo'] = $this->views->loadView("blocks/mapInfo", $_mapInfo, true);

			

			$data['marker'] = new mapItem();
			$data['markerTemplate'] = $this->views->loadTemplate("markerTemplate", 'js', array(), true);
			$data['mapPreviewTemplate'] = $this->views->loadTemplate("mapPreviewTemplate", 'js', array(), true);
			$data['postPreviewTemplate'] = $this->views->loadTemplate("postPreviewTemplate", 'js', array(), true);

		

			$this->views->loadView("map", $data);
		}

		function getPosts()
		{
			$this->loadHelper("image");

			$posts = $this->models['Posts']->getPosts(getPost('latN'), getPost('longN'), getPost('latS'), getPost('longS'), getPost('mapId'), getPost('order'), getPost('limit'), true);
			if ($posts)
			{
				foreach ($posts as $key=>$post)
				{
					if (!file_exists("./uploads/thumbs/".$post['image'].".jpg"))
					{
						@makethumb($post['image'].".jpg", "./uploads/images/", "./uploads/thumbs/", $_width = 160, $_height = 100);
					}
					unset($posts[$key]['userName']);
					unset($posts[$key]['modified']);
					unset($posts[$key]['uploadDate']);
					unset($posts[$key]['userId']);

				}
				
				echo json_encode(array("status"=>"suc", "err"=>"", "msg"=>"", "data"=> $posts));
				//json_encode( utf8_encode($posts));
				return true;
			}
			echo json_encode(array("status"=>"wrn", "err"=>"", "msg"=>"no posts", "data"=> $posts));
		}

		
		public function getPost($postId = false)
		{
			$photoId = $postId? $postId : getGet('pid');
			if (!$photoId)
				return false;
			
			$photo = $this->models['Posts']->getPost($photoId, getGet("mapId"));
			if ($photo)
			{
				$photo = $photo[0];
				$data = $photo;
				$data['pId'] = $photo['id'];
				unset($photo['id']);

				$data['locate'] = false;
				
				if ($this->checkSession() && getUserId() && $data['userId'] == getUserId())
					$data['owner'] = true;
				else
					$data['owner'] = false;
					
				unset($data['userId']);
				
				if (!$postId)
					echo json_encode(array("status"=>"suc", "err"=>"", "msg"=>"", "data"=> $data));
				return $data;
			}
			echo json_encode(array("status"=>"err", "err"=>"", "msg"=>"post not found", "data"=> null));

		}
		
		public function subscribe()
		{
			if(!$this->checkSession())
			{
				echo json_encode(array("status"=>"lgd", "err"=>"", "msg"=>"Not logged in", "data"=> null));				
				return false;				
			}
			$sub = $this->models['Users']->subscribe(getUserId(), getPost('mapId')[0]);
			if ($sub)
			{
				echo json_encode(array("status"=>"suc", "err"=>"", "msg"=>"", "data"=> null));				
			}
		}

	}
