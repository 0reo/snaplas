<?php
	class Comments extends Controllers
	{

		function __construct($_data) {
            parent::__construct($_data);
            $this->loadModel("Comments");
		}
		

		public function getComments()
		{
			$this->checkSession();

			$comments = $this->models['Comments']->getComments(getGet("postId"), getUserId(), getGet("limit"), getGet("offset"));
			if ($comments)
			{

				$return = array("status"=>"suc", "err"=>"", "msg"=>"Successfully loaded comments", "data"=>$comments);
				echo json_encode($return);
				return true;
			}
			$return = array("status"=>"wrn", "wrn"=>"", "msg"=>"No comments for this post", "data"=>null);
			echo json_encode($return);
			return false;
		}
       
        public function postComment()
        {        
			if (!$this->checkSession())
			{
				$return = array("status"=>"err", "err"=>"", "msg"=>"You must be signed in to comment!", "data"=>null);
				echo json_encode($return);
				return false;				
			}
			if (!strlen(getPost("comment")))
			{
				$return = array("status"=>"err", "err"=>"", "msg"=>"No post data", "data"=>null);
				echo json_encode($return);
				return false;				
			}
			
			$success = $this->models['Comments']->insertComment(getPost("postId"), getUserId(), getPost("comment"));

			if ($success)
			{
				$return = array("status"=>"suc", "err"=>"", "msg"=>"Successfully made comment", "data"=>null);
				echo json_encode($return);
				return true;  
			}
			else
			{
				$return = array("status"=>"err", "err"=>"", "msg"=>"Could not post comment", "data"=>null);
				echo json_encode($return);
				return false;					
			}
		}

		function deleteComment()
		{
            $this->checkSession();
            if (getPost('id'))
            {
                $success = $this->models['Posts']->deletePost(getUserId(), getPost('id'));

                if ($success)
                {
                    $return = array("status"=>"suc", "err"=>"", "msg"=>"post successfully deleted", "data"=>null);
                    echo json_encode($return);
                    return false;  
                }
                else
                {
                    $return = array("status"=>"err", "err"=>"", "msg"=>"Error deleting post", "data"=>null);
                    echo json_encode($return);
                    return false;  
                }
            }
		}    

	}
