<?php

	class MobileUpload extends Controllers
	{

		function __construct($_data) {
			parent::__construct($_data);
			$this->loadModel("Users");
			$this->loadHelper("import");
		}

		public function index()
		{	
			if($this->checkSession())
				header("location:/mobileupload/manage");
			else
			{
				$data['css']=array("mobile-main","//fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,800");
				$data['js']=array("jquery-1.9.0.min");
				$this->views->loadView("mobile-login", $data);
			}
			

		}
		public function manage()
		{
			
			$this->loadHelper("image");
			
			if (!$this->checkSession())
			{
				header("location:/mobileupload");
			}
			
			$data['css']=array("mobile-main","leaflet","//fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,800");
			$data['js'] = array('leaflet', 'jquery-1.9.0.min', 'jQuery.XDomainRequest', 'jquery-ui-1.10.4.custom.min', 'perfect-scrollbar.with-mousewheel.min', 'moment.min', "m-main", 'search','uploadFile');
			$data['nav']=$this->views->loadView("blocks/m-nav", null,true);
			$data['maplistitem']=$this->views->loadView("blocks/map-list-item", null,true);
			$data['postlistitem']=$this->views->loadView("blocks/post-list-item", null,true);
			$data['maps'] = array();
			
			$maps = $this->models['Users']->getMaps(getUserId(), getGet("limit"), getGet("offset"), 'newest');
			if ($maps)
			{
				$data['maps'] = $maps;
			}
			$this->views->loadView("mobile-base", $data);
		}
		
		public function map()
		{
			$this->views->loadView("blocks/m-map-post");
		}
		

	}

