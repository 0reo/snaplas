<?php

	class Users extends Controllers
	{

		function __construct($_data) {
			parent::__construct($_data);
			$this->loadModel("Users");
			$this->loadHelper("password");

		}

		public function index()
		{
			//make sure map exsists


		}

		public function checkLogin()
		{

		    // username and password sent from form
		    $myusername=getPost('username');
		    $mypassword=getPost('password');
			
		    //$mypassword = crypt($mypassword, '$6$rounds=5000$'.AUTH_SALT.'$');
		    
		    $loggedin = $this->models['Users']->checkLogin($myusername, $mypassword);
			//print_r($mypassword);

			$this->checkSession();
		    if($loggedin)
		    {
		        // Register $myusername, $mypassword and redirect to file "login_success.php"
				$loggedin = $loggedin[0];
		        $_SESSION['myusername'] = $loggedin['userName'];
		        //$_SESSION['mypassword'] = $loggedin['password'];
		        $_SESSION['myid'] = $loggedin['id'];
		        $_SESSION['age'] = $loggedin['age'];
		        $_SESSION['city'] = $loggedin['city'];
				$_SESSION['country'] = $loggedin['country'];
		        $_SESSION['login'] = 'success';

				$return = array("status"=>"suc", "err"=>"", "msg"=>"Successfully logged in", "data"=>null);
				echo json_encode($return);

		        //header("location:login.php?user=".$_GET['user'].'&imgId='.$_GET["imgId"].'&rank='.$_GET["rank"]);
		    }
		    else
		    {
		        $_SESSION['login'] = 'error';
				$return = array("status"=>"err", "err"=>"", "msg"=>"Incorrect username or password", "data"=>null);
				echo json_encode($return);
		    }            
		}


		public function login()
		{
			if($this->checkSession())
				$this->views->loadView("profile");
			else
				$this->views->loadView("login");			
		}

		public function logout()
		{
			session_start();
			session_unset();
			session_destroy();
			session_write_close();
			setcookie(session_name(),'',0,'/');
			header("location:/");
		}
		
		public function profile()
		{			
			$this->checkSession();
			$this->views->loadView("profile");
		}
		
		public function register()
		{			
			$this->checkSession();
			$user = $this->models['Users']->register(getPost('epassword'), getPost('usernamePro'), getPost('age'), getPost('country'), getPost('city'));
			
			if ($user)
			{
				//echo $user;
				//die($user);
				$this->models['Users']->subscribe($user, 2);
				$this->models['Users']->subscribe($user, 8);
				
				$_SESSION['myusername'] = getPost('usernamePro');
				$_SESSION['myid'] = $user;
				$_SESSION['age'] = getPost('age');
				$_SESSION['city'] = getPost('city');
				$_SESSION['country'] = getPost('country');
				
				$_SESSION['login'] = 'success';
				$return = array("status"=>"suc", "err"=>"", "msg"=>"Successfully registered", "data"=>null);
				echo json_encode($return);
				return true;
			}
			else
			{
				foreach ($GLOBALS['errors']->getErrors("mysql") as $error)
				{
					if ($error['errno'] == 1062)///duplicate entry
					{
						$return = array("status"=>"wrn", "err"=>"1062", "msg"=>"This username is already taken.  Please choose another one and try again", "data"=>null);
						echo json_encode($return);
						return false;
					}
				}
				///if no default errors found
				$return = array("status"=>"err", "err"=>"", "msg"=>"There was an error registering you.  Please try again", "data"=>null);
				echo json_encode($return);
				return false;				
			}  			
		}

		public function updateProfile()
		{	
			$this->checkSession();
			if(!isset($_POST['update']))
				return false;


		    // username and password sent from form
		    $mypassword=getPost('epassword');	///edit password field

			$mypassword? $mypassword = password_hash($mypassword, PASSWORD_DEFAULT) : $mypassword = $mypassword;

			
			$user = $this->models['Users']->update($_SESSION['myid'], getPost('usernamePro'), $mypassword, getPost('country'), getPost('city'));
			
			if ($user)
			{  
				$_SESSION['myusername'] = getPost('usernamePro');
				$_SESSION['city'] = getPost('city');
		        //$_SESSION['age'] = $loggedin['age'];
				$_SESSION['country'] = getPost('country');
		        $_SESSION['login'] = 'success';

				$return = array("status"=>"suc", "err"=>"", "msg"=>"Successfully updated profile", "data"=>null);
				echo json_encode($return);
				return true;
			}
			$return = array("status"=>"err", "err"=>"", "msg"=>"Could not update profile", "data"=>null);
			echo json_encode($return);
			return false;

		}

		public function getPosts()
		{
			$this->loadHelper("image");
			
			if (!$this->checkSession())
			{
				$return = array("status"=>"lgd", "err"=>"", "msg"=>"Not logged in", "data"=>null);
				echo json_encode($return);
				return false;
			}
			$posts = $this->models['Users']->getPosts(getUserId(), getGet('mapId'), getGet("limit"), getGet("offset"), 'newest');
			if ($posts)
			{
				foreach ($posts as $post)
				{
					if (!file_exists("./uploads/thumbs/".$post['image'].".jpg"))
					{
						makethumb($post['image'].".jpg", "./uploads/images/", "./uploads/thumbs/", $_width = 160, $_height = 100);
					}
				}
				$return = array("status"=>"suc", "err"=>"", "msg"=>"Successfully loaded user posts", "data"=>$posts);
				echo json_encode($return);
				return true;
			}
			$return = array("status"=>"err", "wrn"=>"", "msg"=>"Could not load user posts", "data"=>null);
			echo json_encode($return);
			return false;
		}


		public function getSubscriptions()
		{
			if (!$this->checkSession())
			{
				$return = array("status"=>"lgd", "err"=>"", "msg"=>"Not logged in", "data"=>null);
				echo json_encode($return);
				return false;
			}
			$subscriptions = $this->models['Users']->getSubscriptions(getUserId(), getGet("type"), getGet("limit"), getGet("offset"), 'newest');
			if ($subscriptions)
			{

				$return = array("status"=>"suc", "err"=>"", "msg"=>"Successfully loaded user subscriptions", "data"=>$subscriptions);
				echo json_encode($return);
				return true;
			}
			$return = array("status"=>"wrn", "wrn"=>"", "msg"=>"You are not subscribed to any maps!", "data"=>null);
			echo json_encode($return);
			return false;
		}
		
		public function getMaps()
		{
			if (!$this->checkSession())
			{
				$return = array("status"=>"lgd", "err"=>"", "msg"=>"Not logged in", "data"=>null);
				echo json_encode($return);
				return false;
			}
			$maps = $this->models['Users']->getMaps(getUserId(), getGet("limit"), getGet("offset"), 'newest');
			if ($maps)
			{

				$return = array("status"=>"suc", "err"=>"", "msg"=>"Successfully loaded user maps", "data"=>$maps);
				echo json_encode($return);
				return true;
			}
			$return = array("status"=>"wrn", "wrn"=>"", "msg"=>"You do not own any maps!", "data"=>null);
			echo json_encode($return);
			return false;
		}	

		public function requestMap()
		{
			$req = $this->models['Users']->requestMap(getPost('email'));
			if ($req)
			{
				echo json_encode(array("status"=>"suc", "err"=>"", "msg"=>"Thanks for your interest in Snaplas, we'll be in touch shortly", "data"=> null));				
			}
		}		


	}

