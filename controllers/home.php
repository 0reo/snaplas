<?php
	class Home extends Controllers
	{

		function __construct($_data) {
		       parent::__construct($_data);
		}

		public function index()
		{
			$this->libraries['Mobile_Detect']->isMobile() && !$this->libraries['Mobile_Detect']->isTablet() ? header("location:/mobileupload"):$this->views->loadView("splash", array("lgd"=>$this->checkSession()));
			
		}
		
		public function page($pageName)
		{
			$this->views->loadView($pageName);
		}		

	}
