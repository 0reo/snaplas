<?php
	class Posts extends Controllers
	{

		function __construct($_data) {
            parent::__construct($_data);
            $this->loadModel("Posts");
            $this->loadModel("Maps");
			$this->loadHelper("image");
		}

		
		public function upload_file()
		{
            $this->checkSession();

            if (getGet('id'))
            {
				$post = $this->models['Posts']->getPost(getGet('id'), getGet('mid'), getUserId());
                $post = array_shift($post);
            }
            else
			{
				$post = getGet();
				if (count($post['mapId']) > 1)
				{
					$post['showMaps'] = true;
					$post['maps'] = $this->models['Maps']->getMapsById($post['mapId'], getUserId(), array("public", "subscribers")); 
				}
				else
				{
					$post['maps'] = $this->models['Maps']->getMapByName(false, getUserId(), true); 
					$post['name'] = $this->models['Maps']->getMapsById($post['mapId'][0], getUserId(), array("public", "subscribers")); 
					$post['name'] = array_shift($post['name'])['name'];
				}

				$post['mapId'] = array_shift($post['mapId']);
				//die(print_r($post['name'], true));
			}
			!$this->libraries['Mobile_Detect']->isMobile() ? $this->views->loadTemplate('upload_file', "php", $post) : $this->views->loadView('blocks/m-upload_file', $post);

		}
        
        public function postToMap()
        {        
            $this->checkSession();

            $map =  $this->models['Maps']->getMapsById(getPost('mapId'));
			if ($map[0]['posts'] == "owner" && $map[0]['userId'] !== getUserId())
			{
				$return = array("status"=>"err", "err"=>"", "msg"=>"You do not have permission to post to this map", "data"=>null);
				echo json_encode($return);
				return false;  
			}
            $map = array_shift($map)['name'];

			$fileName = false;
			
			if (getFile("file", false))	///handle file uploads
			{
				foreach (getFile("file", false) as $key=>$value):
					if ($value['error'] == 4)
						break;
				
					if ($value['type'] !== 'image/png' 
					&& $value['type'] !== 'image/jpeg' 
					&& $value['type'] !== 'image/gif')
					{
						$return = array("status"=>"err", "err"=>"", "msg"=>$value['tmp_name']." is not a valid image.", "data"=>null);
						echo json_encode($return);
						return false;
					}
					else
					{
						$fileName = "iu_".time().'_'.uniqid().'.'.pathinfo($value['name'], PATHINFO_EXTENSION);
						$fileName = strtolower($fileName);
						if (move_uploaded_file($value['tmp_name'], "./uploads/tmp/$fileName"))
						{
							convertImage($fileName, "./uploads/tmp/", "./uploads/images/", "jpg", 90);
							makethumb($fileName, "./uploads/tmp/", "./uploads/thumbs/", $_width = 160, $_height = 100);
							unlink("./uploads/tmp/$fileName");
						}
						else
						{
							$return = array("status"=>"err", "err"=>"", "msg"=>"file upload error - $fileName - ".$value['error'], "data"=>null);
							echo json_encode($return);
							return false;                          
						}
					}						
				endforeach;
			}
				
			$post = getPost();
			foreach($post as $key=>$val)
			{
				if ($key !== "mapId" &&
				$key !== "title" &&
				$key !== "text" &&
				$key !== "images" &&
				$key !== "rank" &&
				$key !== "pos")
					unset($post[$key]);
			}
			$post['userId'] = getUserId();
			$post['images'] = array($fileName);
            if (getPost('id'))
				$success = $this->models['Posts']->updatePost(getUserId(), getPost('id'),/* getPost('mapId'),*/ getPost('postTitle'), getPost('postText'), $fileName);
            else
				$success = $this->models['Posts']->insertPost($post);

			if ($success)
			{
				$return = array("status"=>"suc", "err"=>"", "msg"=>"", "data"=>null);
				
				getPost('id') ? $return['msg'] = "post successfully updated" : $return['msg'] = "post successfully created";
				$return['data'] = $post;

				echo json_encode($return);
				return true;  
			}
			else
			{
				$return = array("status"=>"err", "err"=>"", "msg"=>"", "data"=>null);
				
				getPost('id') ? $return['msg'] = "Error updating post" : $return['msg'] = "Error creating post";
				$return['data'] = $post;
				echo json_encode($return);
				return false;					
			}
		}

		function deletePost()
		{
            $this->checkSession();
            if (getPost('id'))
            {
                $success = $this->models['Posts']->deletePost(getUserId(), getPost('id'));

                if ($success)
                {
                    $return = array("status"=>"suc", "err"=>"", "msg"=>"post successfully deleted", "data"=>null);
                    echo json_encode($return);
                    return false;  
                }
                else
                {
                    $return = array("status"=>"err", "err"=>"", "msg"=>"Error deleting post", "data"=>null);
                    echo json_encode($return);
                    return false;  
                }
            }
		}
        
        /*function convertImage($originalImage, $outputImage, $quality)
        {
            // jpg, png, gif or bmp?
            $exploded = explode('.',$originalImage);
            $ext = $exploded[count($exploded) - 1]; 

            if (preg_match('/jpg|jpeg/i',$ext))
			{
				$imageTmp=imagecreatefromjpeg($originalImage);
				$exif = exif_read_data($originalImage);
			}
            else if (preg_match('/png/i',$ext))
                $imageTmp=imagecreatefrompng($originalImage);
            else if (preg_match('/gif/i',$ext))
                $imageTmp=imagecreatefromgif($originalImage);
            else if (preg_match('/bmp/i',$ext))
                $imageTmp=imagecreatefrombmp($originalImage);
            else
                return 0;


            if (!empty($exif['Orientation'])) {
                switch ($exif['Orientation']) {
                    case 3:
                    $imageTmp = imagerotate($imageTmp, 180, 0);
                    break;

                    case 6:
                    $imageTmp = imagerotate($imageTmp, -90, 0);
                    break;

                    case 8:
                    $imageTmp = imagerotate($imageTmp, 90, 0);
                    break;
                }
            }

            // quality is a value from 0 (worst) to 100 (best)
            imagejpeg($imageTmp, $outputImage, $quality);
            imagedestroy($imageTmp);
            unlink($originalImage);
            return 1;
        }  */      


		public function massUpload()
		{
 			$this->checkSession();


			$post = getGet();
			if (count($post['mapId']) > 1)
			{
				$post['showMaps'] = true;
				$post['maps'] = $this->models['Maps']->getMapsById($post['mapId'], getUserId(), array("public", "subscribers")); 
			}
			else
			{
				$post['maps'] = $this->models['Maps']->getMapByName(false, getUserId(), true); 
				$post['name'] = $this->models['Maps']->getMapsById($post['mapId'][0], getUserId(), array("public", "subscribers")); 
				$post['name'] = array_shift($post['name'])['name'];
			}

			$post['mapId'] = array_shift($post['mapId']);
			
			

			$this->views->loadTemplate('massUpload', "php", $post);
		}                

	}
