var snapPath = "/home/oreo/public_html/snaplas_clean/"+Date.now()
var debugPath = '/home/oreo/public_html/snaplas';
/*
'leaflet', 'snapmap','profile','posts','main','search','contact','uploadFile'
*/

module.exports = function(grunt)
{
	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),
		svn_export: {
			dev:{
				options:{
					repository:"svn+ssh://oreoprime@throwawaygames.ca/usr/local/svn/snaplas/trunk",
					output:snapPath
				}
			}
		},
		uglify: {
			options: {
				compress: {
					dead_code:true,
					sequences:true,
					drop_debugger:true,
					conditionals:true,
					comparisons:true,
					evaluate:true,
					loops:true,
					unused:true,
					hoist_funs:true,
					if_return:true,
					join_vars:true,
					cascade:true,
					booleans:true,
					warnings:true,
					drop_console:true
					
				},
				mangle: {
					except: ['jQuery'],
					toplevel:true
				}
			},
			dev:{
				files: [{
					dest:debugPath+'/scripts/main.min.js',
					src: [debugPath+'/scripts/snapmap.js',debugPath+'/scripts/profile.js',debugPath+'/scripts/posts.js',debugPath+'/scripts/main.js',debugPath+'/scripts/search.js',debugPath+'/scripts/contact.js',debugPath+'/scripts/uploadFile.js']
				}]
			},
			prod:{
				files: [{
					dest:snapPath+'/scripts/main.min.js',
					src: [snapPath+'/scripts/snapmap.js',snapPath+'/scripts/profile.js',snapPath+'/scripts/posts.js',snapPath+'/scripts/main.js',snapPath+'/scripts/search.js',snapPath+'/scripts/contact.js',snapPath+'/scripts/uploadFile.js']
				}]
			}
		},
		ftpush: {
 			build: {
				auth: {
					host: 'halfbakedideals.com',
					port: 21,
					authKey: 'key2'
				},
			    	src: snapPath,
			    	dest: '/snaplas',
			    	exclusions: ['/**/.DS_Store', '/**/Thumbs.db', '.ftppass', '.htaccess', 'Bram', 'wip', 'uploads/images/*', 'config/database.php'],
				keep: ['/config/database.php', '/b/*', '/uploads/images/*', '/uploads/thumbs/*'],
			    	simple: false,
			    	useList: false
			}
		},
		ftppush_dev: {
				prod:{
				},
				dev:{
				    	src: snapPath,
				    	dest: '/snaplas-dev',
				    	exclusions: ['/**/.DS_Store', '/**/Thumbs.db', '.ftppass', '.htaccess', 'Bram', 'wip', 'uploads/images/*', 'config/database.php'],
					keep: ['/config/database.php', '/b/*', '/uploads/*'],
				    	simple: false,
				    	useList: false
				}
		}
	});

	grunt.loadNpmTasks('grunt-svn-export');
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-ftpush');
	grunt.registerTask('test', ['uglify:dev']);
	grunt.registerTask('default', ['svn_export', 'uglify:prod', 'ftpush']);

}
