<?php
	function isEmail($email)
	{
		if (!preg_match("/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$/i", $email))
		{
			return false;
		}
		else 
		{
			return true;
		}
	}
	$status = "failure";
	$msg = "Error";
	if (!$_POST[name])
	{
		$msg = "Please reenter your name";
	}
	else if (!isEmail($_POST['email']))
	{
		$msg = "Please enter a valid email address";
	}
	else if  (!$_POST['question'])
	{
		$msg = "Please type a question";
	}
	else if ($_POST[name] && isEmail($_POST['email']) && $_POST['question'])
	{
		$headers = 'From: ' .$_POST['name']."<".$_POST['email'].">\r\n" .    'Reply-To: ' .$_POST['email'] . "\r\n" .    'X-Mailer: PHP/' . phpversion();
		if (mail('info@boobmap.com', 'Question from '.$_POST['name'].' for Boobmap', $_POST['question'], $headers))
		{
			$status = "success";
			$msg="message sent";
		}
	}

	echo json_encode(array("status"=>$status, "msg"=>$msg));
?>
