<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=320, initial-scale=1">
<title>OOPS</title>
<link href="./css/error.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="animate.min.css">
<link href='http://fonts.googleapis.com/css?family=Roboto:500,900,100,300,700,400' rel='stylesheet' type='text/css'>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script> 
</head>
<body>
<div id="container">
	<div id="pin-box">
		<div id="circle-box">
			<p id="oops">
				OOPS!
			</p>
		</div>
		<h1 id="error-no">404</h1>
	</div>
	<div id="error-txt-box">
		<h1 id="error-txt">
			Something went wrong! Page not found.
		</h1>
	</div>
</div>
</body>