	<form id="contactForm" action="connect.php" method="post"> 
		<div class="closeBtn">+</div>
		<p style="margin: 10px 5px 0px 5px;">
			Questions?  Concerns?  Just want to say hi?  Then please fill out the form below with 
			any questions or comments you may have for us, and we will get back to you as soon as possible.
		</p>
		<div class="profileField"><input name="name" type="text" id="contactName" placeholder="Full Name"/></div>
		<div class="profileField"><input name="email" type="text" id="contactEmail" placeholder="Email"/> </div>
		<div class="profileField">
			<textarea name="question" id="contactQuestion" placeholder="What's on your mind?..."></textarea>
		</div>
		<input class="button" type="submit" value="submit"/>
	</form>
	<span id="message"></span>

