<?php
	require_once("./config/defines.php");
	require_once("./base/Errors.php");
	global $errors;
	$errors = new Errors();
	$uri = trim($_SERVER['REQUEST_URI'], "/");
	$uri = explode("/", @$uri);
	
	//$function = $uri[count($uri)-1];
	$function = explode("?", $uri[count($uri)-1]);	///get vars from url
	$uri[count($uri)-1] = array_shift($function);

	if(count($uri) == 1 &&  empty($uri[0]))
	{
		$uri[0] = "a";
		$uri[1] = "All";		
	}
	$uri = array_filter($uri);						///remove remaining blanks

	require_once("./config/routes.php");


/*	if(count($uri) == 1 && !empty($uri[0])):
		if (file_exists($uri[0].'.php'))
			require($uri[0].".php");
		else
			require(STATUS_DIR."404.php");
	elseif(count($uri) > 1):

	endif;
*/