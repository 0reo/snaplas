<div class="{{previewType}}">
    <div class="previewTopBar">
        <a href="/m/{{mapNameUrl}}" class="mapLink tooltip" data-tip="Go to this map"><h3 class="mapTitle">{{mapName}}</h3></a>
        <time class="postDate" datetime="{{dateTime}}">Created: {{created}}</time>
    </div>
    <div class="previewImg" ><a href="/m/{{mapNameUrl}}" class="mapLink tooltip" data-tip="Go to this map"><img src="/uploads/images/{{image}}"></a></div>
    <div class="postActions">
		 <menu class="standard">
		 	{{#subscription}}
			<div class="iconBtn standard subscribe submap_{{mapId}}+' {{hoverType}} tooltip" data-tip="Unsubscribe from this map" data-id="{{mapId}}">Subscribed</div><span>{{subCount}}</span>		 	
		 	{{/subscription}}
		 	{{^subscription}}
		 	<div class="iconBtn standard subscribe {{hoverType}} tooltip" data-tip="Subscribers" data-id="{{mapId}}"><span>{{subCount}}</span></div>
		 	{{/subscription}}
		 	<div class="iconBtn standard share shareMap tooltip" data-tip="Share this map" data-name="{{mapName}}"></div>
		 </menu>	
    </div>
</div>