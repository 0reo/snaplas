
		<div class="panelTop">
			<div class="closeBtn"></div>	
			<h4 class="panelTitle left">Post To: <span>{{name}}</span></h4>
		</div>
		{{#showMaps}}
		<div id="selectMap">
		{{/showMaps}}
		{{^showMaps}}
		<div id="selectMap" style="display:none;">
		{{/showMaps}}
			{{#maps}}
				<div class="mapPreview" data-id="{{id}}">
					<div class="previewImg" ><img src="/uploads/images/{{image}}"></div>
					<div class="previewInfo">
						<h3 class="mapTitle">{{name}}</h3>
						<time class="postDate" datetime="{{created}}">Created: {{created}}</time>
					</div>

				</div>
			{{/maps}}
		</div>

		<form action="/posts/postToMap" method="post" enctype="multipart/form-data" name="photoForm" id="photoForm">
			<menu class="large">
				<div class="iconBtn standard sideBtn makePhotoPostBtn" id="makePhotoPost" title="About this post"></div>    
			</menu>
			<div id="post-a-post-image-upload">
				<input type="file" name="file[]" id="file" multiple/>
				<img id="preview"{{#image}} src="/uploads/images/{{image}}.jpg"{{/image}}/>
				{{^image}}Drag and Drop<br><br>Files Here{{/image}} 		
			</div>
			<select name="location">
				<option value="[-34.0148157, -33.6948157, 151.0564539, 151.3764539]">Sydney</option>
				<option value="[-27.6289682, -27.3089682, 152.8634991, 153.1834991]">Brisbane</option>
			</select>
			<input type="hidden" name="mapId" value="{{mapId}}">
			<br>
			<input type="button" class="button" id="changeMap" value="Change Map"/>	
			<input type="submit" name="submit" value="Post" class="button"/>
		</form>
