<section class="popup" id="bottomPanel">
    <div class="panelTop">
        
        <div id="postInfoLeft">
			<h4 class="mapTitle"><a href="{{mapName}}">{{mapName}}</a></h4><br>
			<h3 class="postUser">by {{userName}}</h3>
		</div>
		
        <div class="settings" title="Settings">
            <ul class="settingsMenu postMenu">
                <li data-id="{{id}}" class="editBtn"><a href="#">Edit</a></li>
                <li data-id="{{id}}" class="deleteBtn"><a href="#">Delete</a></li>
                <li data-id="{{id}}" class="reportBtn"><a href="#">Report</a></li>                
            </ul>
        </div>
		<h4 class="panelTitle">{{title}}</h4><time class="postDate" datetime="{{datetime}}">{{uploadDate}}</time>
    </div>
    <menu class="large" id="panelInfoWindow">
        <!--<span class="panelInfo"><span id="likeCount"></span>likes</span><span class="panelInfo"><span id="viewCount"></span>views</span>
        <div class="iconBtn standard sideBtn" id="likes">
            Like
        </div>-->
        <a href="" class="iconBtn standard sideBtn commentBtn" id="commentBtn" title="Comments"></a>
		<div class="iconBtn standard sideBtn showPhotoDescBtn" id="showPhotoDesc" title="About this post"></div>
        <div class="iconBtn standard sideBtn share" style="float:right;" title="Share this post"></div>
    </menu>
    <div id="panelBottom">
        <div id="postContent">{{#image}}<img src="/uploads/images/{{image}}.jpg">{{/image}}</div>
        <pre id="photoDesc">{{text}}</pre>
        
    </div>
</section>