<a href="#pid={{markerId}}">
<svg class="customMark" version="1.1" id="{{markerId}}" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 width="102px" height="127.561px" viewBox="0 0 102 127.561" xml:space="preserve">

	<defs>
		<pattern id="{{patternId}}" x="0" y="-25.0" patternUnits="userSpaceOnUse" height="115%" width="115%">
			<image height="115%" width="135%"  xlink:href="/uploads/thumbs/{{imageUrl}}"></image>
		</pattern>
		<linearGradient id="SVGID_{{markerId}}_" gradientUnits="userSpaceOnUse"  x1="80.9995" y1="0.4995" x2="80.9995" y2="41.5005">
			<stop offset="0" style="stop-color:#00B0FF"/>
			<stop offset="1" style="stop-color:#0091EA"/>
		</linearGradient>		

		<set xlink:href="#{{markerId}}path1" fill="freeze" begin="indefinite" id="{{markerId}}ani1" attributeName="d" to="M128,28.475c0.001-0.607,0-7.475-7.68-7.475c-0.113,0-112.682,0-112.795,0C0,21,0,27.91,0,28.475
	c0,10.067,0,45.763,0,54.965l0,0v54.121l0,0c0,0.511,0,7.494,7.55,7.439h8.436h16.035h15.962h7.167h16.405h8.425h15.961h16.184
	c0.465,0,7.754-0.003,8.195,0c7.68,0.055,7.68-6.898,7.68-7.439l0,0V83.439l0,0C128,74.412,127.989,38.193,128,28.475z"/>

		<set xlink:href="#{{markerId}}previewWindow" fill="freeze" begin="{{markerId}}ani1.begin" id="{{markerId}}ani2" attributeName="d" to="M128,28.475c0,23.722,0,60.527,0,80.549h-128c0-15.713,0-56.523,0-80.549
	c0-0.447,0-7.475,7.525-7.475c0.304,0,112.493,0,112.796,0C129.936,21,129.936,27.88,129.936,28.475z"/>

		<set xlink:href="#{{markerId}}iconBorder" fill="freeze" begin="{{markerId}}ani1.begin" id="{{markerId}}ani3" attributeName="d" to="M128,0c-11.599,0-21,9.402-21,21s9.401,21,21,21s21-9.402,21-21S139.599,0,128,0z
		 M128,41.5c-11.321,0-20.5-9.178-20.5-20.5S116.679,0.5,128,0.5s20.5,9.178,20.5,20.5S139.321,41.5,128,41.5z"/>

		<set xlink:href="#{{markerId}}iconGrad" fill="freeze" begin="{{markerId}}ani1.begin" id="{{markerId}}ani4" attributeName="cx" to="128"/>


		<set xlink:href="#{{markerId}}path1" fill="freeze" begin="indefinite" id="{{markerId}}ani6" attributeName="d" to="M 90,48.04 C 90,23.587 70.035,3.743 45.34,3.562 45.227,3.561 45.113,3.561 45,3.561 20.148,3.561 0,23.474 
	0,48.04 0,57.737 3.148,66.703 8.477,74.012 H 8.463 l 32.457,51.263 h 0.014 c 0.834,1.363 2.335,2.286 4.066,2.286 l 0,0 0,0 0,0 0,0 0,0 0,0 0,0 0,0 c 0.465,0 
	0.914,-0.066 1.337,-0.19 1.152,-0.338 2.12,-1.099 2.731,-2.096 h 0.018 L 81.536,74.012 H 81.522 C 86.849,66.702 90,57.736 90,48.04 z"/>

		<set xlink:href="#{{markerId}}previewWindow" fill="freeze" begin="{{markerId}}ani6.begin" id="{{markerId}}ani7" attributeName="d" to="M88.5,47.999c0,23.722-18.987,43.008-42.591,43.492C45.606,91.497,45.304,91.5,45,91.5
		C20.975,91.5,1.5,72.024,1.5,47.999C1.5,23.975,20.975,4.5,45,4.5c0.304,0,0.607,0.003,0.91,0.009
		C69.512,4.994,88.5,24.279,88.5,47.999z"/>

		<set xlink:href="#{{markerId}}iconBorder" fill="freeze" begin="{{markerId}}ani6.begin" id="{{markerId}}ani8" attributeName="d" to="M81,0C69.401,0,60,9.402,60,21s9.401,21,21,21s21-9.402,21-21S92.598,0,81,0z M81,41.5
		c-11.321,0-20.5-9.178-20.5-20.5S69.678,0.5,81,0.5s20.5,9.178,20.5,20.5S92.321,41.5,81,41.5z"/>

		<set xlink:href="#{{markerId}}iconGrad" fill="freeze" begin="{{markerId}}ani6.begin" id="{{markerId}}ani9" attributeName="cx" to="81"/>		

	</defs>


	<path id="{{markerId}}path1" fill="#F5F5F5" d="M 90,48.04 C 90,23.587 70.035,3.743 45.34,3.562 45.227,3.561 45.113,3.561 45,3.561 20.148,3.561 0,23.474 
	0,48.04 0,57.737 3.148,66.703 8.477,74.012 H 8.463 l 32.457,51.263 h 0.014 c 0.834,1.363 2.335,2.286 4.066,2.286 l 0,0 0,0 0,0 0,0 0,0 0,0 0,0 0,0 c 0.465,0 
	0.914,-0.066 1.337,-0.19 1.152,-0.338 2.12,-1.099 2.731,-2.096 h 0.018 L 81.536,74.012 H 81.522 C 86.849,66.702 90,57.736 90,48.04 z"/>

	<path class="previewWindow" id="{{markerId}}previewWindow" fill="url(#{{patternId}})" d="M88.5,47.999c0,23.722-18.987,43.008-42.591,43.492C45.606,91.497,45.304,91.5,45,91.5
		C20.975,91.5,1.5,72.024,1.5,47.999C1.5,23.975,20.975,4.5,45,4.5c0.304,0,0.607,0.003,0.91,0.009
		C69.512,4.994,88.5,24.279,88.5,47.999z"/>
	<g>
		{{#numMarkers}}
			<path id="{{markerId}}iconBorder" fill="#01579B" d="M81,0C69.401,0,60,9.402,60,21s9.401,21,21,21s21-9.402,21-21S92.598,0,81,0z M81,41.5
		c-11.321,0-20.5-9.178-20.5-20.5S69.678,0.5,81,0.5s20.5,9.178,20.5,20.5S92.321,41.5,81,41.5z"/>
			<circle id="{{markerId}}iconGrad" fill="url(#SVGID_{{markerId}}_)" cx="81" cy="21" r="20.5"/>
			<text x="81" y="26" text-anchor="middle" font-size="13" fill="#FFFFFF" >{{numMarkers}}</text>
		{{/numMarkers}}
		{{^numMarkers}}
			<path id="{{markerId}}iconBorder" fill="#01579B" d="M81,0C69.401,0,60,9.402,60,21s9.401,21,21,21s21-9.402,21-21S92.598,0,81,0z M81,41.5
		c-11.321,0-20.5-9.178-20.5-20.5S69.678,0.5,81,0.5s20.5,9.178,20.5,20.5S92.321,41.5,81,41.5z"/>
			<circle id="{{markerId}}iconGrad" fill="url(#SVGID_{{markerId}}_)" cx="81" cy="21" r="20.5"/>
		{{/numMarkers}}
	</g>
</svg>
</a>