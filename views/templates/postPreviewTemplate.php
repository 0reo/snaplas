<div class="{{previewType}}">
    <div class="previewTopBar">
        <a class="postLink tooltip" data-tip="View post" data-id="{{postId}}" href="#"><h3 class="postTitle">{{postTitle}}</h3></a>
        <time class="postDate" datetime="{{dateTime}}">Posted: {{created}}</time>
        <div class="settings">
			<ul class="settingsMenu editPost">
				<li data-id="{{postId}}" class="editBtn"><a href="#">Edit</a></li>
				<li data-id="{{postId}}" class="deleteBtn"><a href="#">Delete</a></li>
			</ul>
		</div>
    </div>
    <div class="previewImg" ><a class="postLink tooltip" data-tip="View post" data-id="{{postId}}" href="#"><img src="/uploads/thumbs/{{image}}"></a></div>
    <div class="postActions">
		<a href="/m/{{mapNameUrl}}"><h4 class="mapTitle tooltip" data-tip="Go to this map">{{mapName}}</h4></a>
        <menu class="standard"><div class="iconBtn standard multi" data-id="{{postId}}"></div></menu>
        <menu class="standard"><div class="iconBtn standard share tooltip" data-tip="Share this post" data-mapname="{{mapNameUrl}}" data-id="{{postId}}"></div></menu>
    </div>
</div>