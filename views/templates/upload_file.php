<div class="panelTop">
	<div class="closeBtn"></div>	
	<h4 class="panelTitle left">Post To: <span>{{name}}</span></h4>
</div>
{{#showMaps}}
<div id="selectMap">
{{/showMaps}}
{{^showMaps}}
<div id="selectMap" style="display:none;">
{{/showMaps}}
	{{#maps}}
		<div class="mapPreview" data-id="{{id}}">
			<div class="previewImg" ><img src="/uploads/images/{{image}}"></div>
			<div class="previewInfo">
				<h3 class="mapTitle">{{name}}</h3>
				<time class="postDate" datetime="{{created}}">Created: {{created}}</time>
			</div>

		</div>
	{{/maps}}
</div>

<form action="/posts/postToMap" method="post" enctype="multipart/form-data" name="photoForm" id="photoForm">
    <menu class="large">
		<div class="iconBtn standard sideBtn makePhotoPostBtn" id="makePhotoPost" title="About this post"></div>    
    </menu>
	<div id="post-a-post-image-upload">
		<input type="file" name="file" id="file" />
		<img id="preview"{{#image}} src="/uploads/images/{{image}}.jpg"{{/image}}/>
		{{^image}}Drag and Drop<br><br>Files Here{{/image}} 		
	</div>
	<input type="text" name="postTitle" class="postTitle" placeholder="Title" value="{{title}}">
	<textarea name="postText" placeholder="Photo Description">{{text}}</textarea>
	<input type="hidden" name="lat" value="{{lat}}">
	<input type="hidden" name="lng" value="{{lng}}">
	<input type="hidden" name="mapId" value="{{mapId}}">
	<input type="hidden" name="id" value="{{id}}">
	<br>
	<input type="button" class="button" id="changeMap" value="Change Map"/>	
	<input type="submit" name="submit" value="Post" class="button"/>
</form>