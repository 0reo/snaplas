    <div class="closeBtn"></div>
    <h3>Copy this link to share it</h3><input type="text" value="%%shareUrl%%">
    <div class="fb-share-button" data-href="%%shareUrl%%" data-type="button" data-width="100" style="vertical-align:super; margin-right:5px; zoom:1;*display:inline"></div>
    <a href="https://twitter.com/share" class="twitter-share-button" data-url="%%shareUrl%%" data-text="%%shareText%% - " data-via="snaplas" data-size="large" data-count="none" data-dnt="true">Tweet</a>