<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=320, initial-scale=1">
	<title>Snaplas</title>
	<link href="/css/splash.css" rel="stylesheet" type="text/css">
	<link rel="image_src" href="/images/icon.png" />
	<link href="/css/snaplas-theme/jquery-ui-1.10.4.custom.min.css" rel="stylesheet" type="text/css">
	<link href='//fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,800' rel='stylesheet' type='text/css'>
	<script src="/scripts/jquery-1.10.2.js"></script> 
	<script src="/scripts/jquery-ui-1.10.4.custom.min.js"></script> 
	<meta name="description" content="Snaplas is an atlas to discover pictures, stories and moments">
</head>

<body> 
<div class="splash-container" id="splash-bg">
	<header id="header-container"> 
		<a href="/" id="logo-container"></a>
		<div id="sign-in-btn">
			<?=$lgd?"Profile":"Sign In"?>
		</div>
		<div class="maps-btn">
			Featured Maps
		</div>
		<div class="maps-menu-closed">
			<ul>
				<li><a href="/m/Toronto" title="A place to create moments, discover stories, and get engaged with your Toronto community.">Toronto</a></li>
				<li><a href="/m/Stuff" title="Anything Goes!">Stuff</a></li>
				<li><a href="/m/Terra" title="An open map where you can post images of natural landscapes around the world.">Terra</a></li>
				<li><a href="/m/Paranormal" title="Paranormal">Paranormal</a></li>
				<li><a href="/m/Liv-On-Air" title="The road to radio.">Liv On Air</a></li>
				<li><a href="/m/ENDhatelaws" title="#Endhatelaws">#ENDhatelaws</a></li>
				<li><a href="/m/snaplas" title="Official Snaplas Map">Snaplas</a></li>
				<li><a href="/m/bram" title="Bram Zeidenberg">Bram</a></li>
				<li><a href="/m/Adam-Clarke" title="Adam Clarke">Adam Clarke</a></li>
		
			</ul>
		</div>
	</header>
	<div class="main-container">
		<div class="blurb-container">
			<h1 class="blurb-text-large">
				Snaplas is an atlas to discover pictures, stories and moments
			</h1>
			<h2 class="blurb-text-medium">
				Create, Discover and Engage with the latest featured map:
			</h2>
			<a href="/m/Toronto" class="main-map-btn">Toronto</a>
			<?php if ($lgd):?>
				<h2 class="blurb-text-medium">
					Want to create a map of your very own?<br> 
					Request early access
				</h2>
				<form class="form-container" id="requestForm" name="requestForm">
					<input id="form-text-box" type="text" name="Email Form"  placeholder="Your Email" required>
					<input id="requestSub" class="form-submit-btn " type="submit" value="Request Map">
				</form>
			<?php else:?>
				
			<?php endif;?>
		</div>
	</div>
	<footer id="footer-container">
		<div id="copyright">&copy; 2014 Snaplas Inc.</div>
		<div id="legal-container">
			<!--<a href="/howto" class="legal-btn aboutBtn">How To</a>-->
			<a href="/about" class="legal-btn aboutBtn">About</a>
			<a href="/privacy" class="legal-btn privacyBtn">Privacy</a>				
			<a href="/terms" class="legal-btn termsBtn">Terms</a>
		</div>
		<div id="social-container">
			<a href="https://twitter.com/Snaplas" class="social-btn"  id="twtr" target="_blank"></a>
			<a href="https://www.facebook.com/snaplas?fref=ts" class="social-btn"  id="fb" target="_blank"></a>
			<a href="mailto:info@snaplas.com" class="social-btn"  id="eml"></a>				
		</div>
	</footer>
</div>
<div id='profileBox'></div>
<div class="popup" id="info"></div>
<?php include "analytics.php"?>

<script type="text/javascript">
	ga('send', 'pageview');
	var lgd=<?=$lgd?"true":"false"?>;
</script>
<script src="/scripts/splash.js"></script>
</body>
