<div class="panelTop">
    <div class="closeBtn"></div>
    <h4 class="panelTitle">Snaplas.com Privacy</h4>
</div>
<div id="wrapper">
    <h4>Privacy Policy</h4>
    <p>We at Snaplas Inc recognize that your personal privacy is important.  To that end, we are pleased to provide you with the following privacy policy applicable to http://www.snaplas.com (the “Website”).  Please be aware that this privacy policy (the “Privacy Policy”) is subject to change and we advise you to check back here from time to time.  Also, while we do provide you with links to third-party websites as a service, we are not responsible for the content or privacy policies or data collection practices of any such third party pages or sites that you may access from time to time.  Before submitting any information on those third-party sites, please review and understand their privacy policies since they differ from site to site.
    </p>
    <h4>Collected Information</h4>
    <p>We only collect information that you voluntarily provide to us with your consent and knowledge.  Any information that you may submit for the purpose of searching for events, subscribing to an e-mail or newsletter service or participation in a discussion forum, or other reasons, is used by us to facilitate the service being provided  We may also obtain, from time to time, information through automatic tracking of IP addresses and browser types for systems administration purposes, in addition to information about the Website, features, links or other data about site browsing and purchasing (“Information”).
    </p>
    <h4>Use Of Information</h4>
    <p>Snaplas Inc does not sell any Information collected from our users.  Information that we collect from forums, surveys or questionnaires or the like that are voluntarily submitted by you and other users, will be used to improve your Website experience.  We may, however, share information with certain companies who may offer to you services and products of interest.  If you do not wish to have your information shared, please e-mail us at info@snaplas.com.   We may send e-mail messages to you from time to time to keep you informed about new developments, or changes to the Website or such things as events or new services available on our Website.  If you are not interested in receiving such e-mail messages, you may unsubscribe or indicate that you are not interested in receiving e-mail information when you register to use our Website or by changing your personal profile.  Other than the foregoing, and in those cases where we must comply with applicable laws, subpoenas or other legal proceedings, we will not disclose Information to other parties without your prior consent.
    </p>
    <h4>Cookies</h4>
    <p>We may place a “cookie” on your computer so that we may provide you with a better browsing experience while visiting the Website.  Other “cookies” may be placed on your computer by us or our advertisers to gather broad non-identifiable demographic information about users of our Website.  If you do not want us or others to place “cookies” on your computer, most internet web browsers include a function that will allow you to stop “cookies” from being placed on your computer.  Please see your instruction manual or help feature for your browser on how to do this.
    </p>
    <h4>Consent</h4>
    <p>By using this Website and submitting any Information, you hereby consent to the collection, use and storage of your information by Snaplas Inc, its affiliates, related companies, joint venturers, partners, suppliers, licensees and their respective shareholders, directors, officers, employees, agents, licensees and assigns in accordance with this policy.  We encourage you to review our privacy policy whenever you use this Website to make sure you understand how your information will be used.
    </p>
    <h4>Information Requests</h4>
    <p>At your request, we will use our best efforts to advise you what Information we have, what it is being used for, and to whom it has been disclosed.  Please e-mail our Privacy Policy Contact Person at info@snaplas.com with any request you may have.
    </p>
    <h4>Feedback</h4>
    <p>Please feel free to provide us with any feedback, comments, questions or concerns that you may have about this Privacy Policy.  You can email our Privacy Policy Contact Person at info@snaplas.com. 
    </p>
</div>