<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=320, initial-scale=1">
<title>Snaplas Mobile</title>
<link href="/css/mobile-main.css" rel="stylesheet" type="text/css">
<link href='//fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,800' rel='stylesheet' type='text/css'>
<script src="/jquery.min.js"></script> 
</head>

<body>
<main id="main-box">
	<div id="login-logo-box">
    	<div id="login-logo-mobile">mobile</div>
    </div>
    <div id="spalsh-container">
    	<div id="spalsh-text-1">
        	To get the best experience possible, Snapals is currently availible for desktop only. A mobile version is in development.
        </div>
        <!--<div id="spalsh-text-2">
        	If you are one of our registered content creators please sign in below to upload new posts to your maps:
        </div>
        <div id="login-sign-in-btn">Sign In</div>-->
    </div>
    
</main>
</body>
</html>