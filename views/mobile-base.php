<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="viewport" content="minimal-ui, width=device-width, height=device-height, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
<title>Snaplas Mobile</title>
<?= loadCSS ($css)?>
<script type="text/javascript">
	var maplisttemplate = '<?=$maplistitem ?>';
	var postlisttemplate = '<?=$postlistitem ?>';
	var maps = <?=json_encode($maps);?>
</script>
</head>

<body>
<main id="main-box">
	<?=$nav ?>
	<section id="list-container" class="map-list-container"></section>
	<section id="post-new">New Post</section>
	<section id="other-container"></section>
	<section id="map-container"></section>
</main>
<?= loadJS ($js)?> 
</body>
</html>
