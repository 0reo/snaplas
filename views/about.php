<div class="panelTop">
    <div class="closeBtn"></div>
    <h4 class="panelTitle">About</h4>
</div>
<div id="wrapper">

    <h4>What is Snaplas?</h4>
    <p>    Snaplas is a microblogging platform that offers a new location-based perspective on the content that interests you. The Snaplas microblogging platform allows content to be published and discovered on user-generated maps. You can tailor your experience with map subscriptions and engage with content by posting, commenting, and sharing.

    Discovering content across a map is novel but also practical. The Snaplas user-experience mimics real-life discovery. We don’t scan a room up and down. We see the room as a whole looking in multiple directions to focus on the different interesting aspects of the room. That’s how Snaplas works.
    </p> 
    <h4>What you can do on Snaplas right now:</h4><ul><li>Create snapshots on open maps like <a href="/m/Toronto">this one</a></li>
        <li>Discover snapshots posted by other users</li>
        <li>Discover maps. Featured maps are listed at the top of the page</li>
        <li>Subscribe to maps that interest you</li>
        <li>Engage with the your community by commenting on snapshots</li>
        <li>Share snapshots and maps to social networks like Twitter and Facebook</li>
        <li>Use mobile web to post and edit your snapshots</li>
        <li>Manage your profile, snapshots, and subscriptions</li>
    </ul> 
    <h4>The Company</h4>
    <p>Snaplas Inc. is a web startup from Toronto, Canada that specializes in developing location-based software. Snaplas was conceptualized in October 2013 and incorporated in March 2014. The core team consists of Adam Clarke, Bram Zeidenberg & Viljar Vali.
    </p>
    <br><br>
</div>