<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
		<title>Snaplas Mobile</title>
		<?= loadCSS ($css)?>
		<?= loadJS ($js)?> 
	</head>

	<body>
		<main id="main-login">
			<div id="login-logo-box">
				<div id="login-logo-mobile">mobile</div>
			</div>
			<div id="login-form-container">
				<form action="/checkLogin" method="post" name="login">
					<input class="login-form-box" type="text" name="username" id="username" placeholder="username">
					<input class="login-form-box" type="password" name="password" id="password"  placeholder="password">
					<div class="errorMsg" id="loginError"></div>					
					<input id="login-sign-in-btn" name="signIn" type="submit" value="Sign In">
				 </form>
			</div>
			<div id="login-use-description">
				Upload and manage posts with your mobile device.  Registration is available on the desktop site.
			</div>
		</main>
		<script type="text/javascript">

			$("input").on("change", function(){
				$("#loginError").html('');
			})

			$("form[name=login]").on("submit", function(e){
				e.preventDefault();

				$.ajax({
					type: "POST",
					url: "/users/checkLogin",
					dataType:"json",
					data: {
						username:$('#username').val(),
						password:$('#password').val()
					},
					success: function(data) {
						console.log(data);
						if (data.status == 'suc')
						{
							document.location = "/mobileupload/manage";
						}
						else
							$("#loginError").html(data.msg);
					}
				});
			})



		</script>			
	</body>
</html>