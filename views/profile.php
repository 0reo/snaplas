<?php
	require_once CONFIG_DIR.'database.php';
	$submit;
	$logout;
	
	//print_r($_SESSION);
	//die();
	
	if(!isset($_SESSION['myusername']))
	{
		$submit = '<div class="profileField"><input type="checkBox" id="agreeTerms" required><span style="vertical-align: middle; font-size: 1rem;">I agree to the Snaplas <a href="#terms" class="termsBtn">Terms of Service</a> and <a href="#privacy" class="privacyBtn">Privacy Policy</a></span></div>
		<input type="hidden" name="register" value="true">
			<input class="button" type="submit" id="registerSubBtn" value="register" />';
		$action = 'profile';
	}
  else
  {
  		$submit = '<input type="hidden" name="update" value="true">
			<input class="button" type="submit" id="updateProfile" value="Update" />';
		$action = 'profile';
  }


?>

	<form id="profileForm" method="post">
  <div id="profileInfo">

  
		<div class="profileField"><input  type="text" name="usernamePro" id="usernamePro" placeholder="Username" value="<?=safeValue($_SESSION['myusername'])?>" pattern=".{4,}" required title="Username must be at least 4 characters long"></div>
		<div class="profileField"><input  name="epassword" id="epassword" placeholder="Password" value="" type="password"  pattern=".{6,}" <?=!safeValue($_SESSION['myusername'])?"required":""?> title="Password must be at least 6 characters long"/></div>
		<div class="profileField"><input  name="password2" id="password2" placeholder="Confirm Password" value="" type="password" /></div>
		<div class="profileField"><input  type="text" name="age" id="age" placeholder="DOB" value="<?=safeValue($_SESSION['age'])!=='0000-00-00'?safeValue($_SESSION['age']):''?>" /></div>

		<div class="profileField">
		<select name="country" id="country">
			<option<?=isset($_SESSION['country']) && $_SESSION['country'] == "Canada"?" selected":""?>>Canada</option>
			<option<?=isset($_SESSION['country']) && $_SESSION['country'] == "United States of America"?" selected":""?>>United States of America</option>
			<option<?=isset($_SESSION['country']) && $_SESSION['country'] == "Other"?" selected":""?>>Other</option>
		</select>
    </div>
		<div class="profileField"><input  type="text" name="city" id="city" placeholder="City" value="<?=safeValue($_SESSION['city'])?>" /></div>
		<?php
      echo $submit;
  	  if(isset($_SESSION['login']) && $_SESSION['login'] === 'success')
        echo '<input type="button" class="button" id="logoutBtn" value="Logout" onclick="window.location = \'/users/logout\';">';
  ?>
    </div>
	</form>	
	<span id="message"></span>
