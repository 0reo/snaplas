<?php
	if (isset($_SESSION['login']))
		switch($_SESSION['login'])
		{
			case 'error':
			{
				$_SESSION['login'] = "";
				break;
			};
		}
?>

<form name="login"  method="post" action="/users/checkLogin">
	<input class="loginField" name="username" id="username" required placeholder="username"/>
	<input class="loginField" name="password" id="password" type="password" required placeholder="password"/>
	<input id="registerBtn" class="loginBtn button" type="button" value="register" />
	<input class="loginBtn button" style="float:right;" type="submit" value="Login" onclick=""/>
</form>

<div class="errorMsg" id="loginError"></div>


<script type="text/javascript">

	$("form[name=login]").on("submit", function(e){
		e.preventDefault();
		
		$.ajax({
			type: "POST",
			url: "/users/checkLogin",
			dataType:"json",
			data: {
				username:$('#username').val(),
				password:$('#password').val()
			},
			success: function(data) {
				if (data.status == 'suc')
				{
					document.location.reload(true);
				}
				else
					$("#loginError").html(data.msg);
			}
		});


	})


</script>