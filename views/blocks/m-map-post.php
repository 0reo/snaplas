<div id="post-a-post-map-prompt">
	Post to <span id="mapName">NO MAP SELECTED</span>
</div>
<div id="map"></div>
<div id="post-a-post-btn">Post</div> 
<script type="text/javascript">

		var tileUrl = '//{s}.mqcdn.com/tiles/1.0.0/map/{z}/{x}/{y}.jpg',
		attr = '© OpenStreetMap contributors<br>Tiles Courtesy of <a href="http://www.mapquest.com/" target="_blank">MapQuest</a> <img src="developer.mapquest.com/content/osm/mq_logo.png">',
		tileLayer = new L.TileLayer(tileUrl, {
			maxZoom: 18,
			subdomains: ["otile1-s","otile2-s","otile3-s","otile4-s"],
			attribution: attr
		});

		var droppin = L.icon({
		    iconUrl: '/images/marker.png',

		    iconSize:     [38, 38], // size of the icon
		    iconAnchor:   [19, 38], // point of the icon which will correspond to marker's location
		});

		var map = new L.Map('map');
		var marker = null;
		map.attributionControl.setPrefix('');
		map.attributionControl.setPosition('bottomleft');
		map.addLayer(tileLayer);
		map.locate({setView: true, maxZoom: 15});


		map.on('click', function(e){
			marker.setLatLng(e.latlng);
			$("input[name=lat]").val(e.latlng.lat);
			$("input[name=lng]").val(e.latlng.lng);
			/*if (uploadPhoto)
			{
				e.latlng;
				obj.uploadPhoto = false;
				$.get('/posts/upload_file',
				{
					lat:e.latlng.lat,
					lng:e.latlng.lng,
					mapId:obj.mapId
				}, function(data){
					$("#upload").html(data).toggle("slow");
				});
			}*/
//			ga('send', 'pageview', window.location.pathname.replace(/\/[0-9]+$/, "").replace(/\/$/, "") );	///google analytics
			
		}).on('locationfound', function(e){
			marker = L.marker(e.latlng,  {icon: droppin}).addTo(map);
			$("input[name=lat]").val(e.latlng.lat);
			$("input[name=lng]").val(e.latlng.lng);

		}).on('locationerror', function(e){
		});
		
		
//		ga('send', 'pageview');
		
	
</script>
