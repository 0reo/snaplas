<div class="closeBtn"></div>
<form action="/posts/postToMap" method="post" enctype="multipart/form-data" name="photoForm" id="photoForm">

	<input type="text" name="postTitle" class="postTitle" placeholder="Title" value="<?=safeValue($title)?>">
	<input type="button" name="upload" value="Upload Image" id="uploadBtn" class="button"/>

	<input type="file" name="file" id="file" />
	<br />
	<input type="hidden" name="lat" value="<?=safeValue($lat)?>">
	<input type="hidden" name="lng" value="<?=safeValue($lng)?>">
	<input type="hidden" name="mapId" value="<?=safeValue($mapId)?>">
	<input type="hidden" name="id" value="<?=safeValue($id)?>">
	<img id="preview" src="<?php echo empty($image)?'':'/uploads/images/'.$image.'.jpg';?>"/>
	<textarea name="postText" placeholder="Photo Description"><?=safeValue($text)?></textarea>
	<br>
	<input type="submit" name="submit" value="Submit" class="button"/>
</form>