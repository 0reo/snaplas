<div id="mapbar-container">
	<div id="mapbar-title">
		Featured Maps:
	</div>
	<div class="mapbar-maps-container">
		<a href="/m/Toronto" class="mapbar-maps" title="A place to create moments, discover stories, and get engaged with your Toronto community.">Toronto</a>
		<span class="mapbar-maps-sep">-</span>
		<a href="/m/Stuff" class="mapbar-maps" title="Anything Goes!">Stuff</a>
		<span class="mapbar-maps-sep">-</span>	
		<a href="/m/Terra" class="mapbar-maps" title="An open map where you can post images of natural landscapes around the world.">Terra</a>
		<span class="mapbar-maps-sep">-</span>
		<a href="/m/Paranormal" class="mapbar-maps" title="Paranormal">Paranormal</a>
		<span class="mapbar-maps-sep">-</span>	
		<a href="/m/Liv-On-Air" class="mapbar-maps" title="The road to radio.">Liv On Air</a>
		<span class="mapbar-maps-sep">-</span>
		<a href="/m/ENDhatelaws" class="mapbar-maps" title="#Endhatelaws">#ENDhatelaws</a>
		<span class="mapbar-maps-sep">-</span>
		<a href="/m/Snaplas" class="mapbar-maps" title="Official Snaplas Map">Snaplas</a>
		<span class="mapbar-maps-sep">-</span>
		<a href="/m/Bram" class="mapbar-maps" title="Bram Zeidenberg">Bram</a>
		<span class="mapbar-maps-sep">-</span>
		<a href="/m/Adam-Clarke" class="mapbar-maps" title="Adam Clarke">Adam Clarke</a>
	</div>
	
</div>