
<form id="post-a-post-form-size" action="Post a Post" method="post"> 
	<div id="post-a-post-container">
		<input class="post-a-post-title" name="postTitle" id="postTitle" type="text" placeholder="Title" value="<?=safeValue($title)?>">
		<div id="post-a-post-image-upload">
			<img id="preview"<?= empty($image)?'Upload an Image':' src="/uploads/images/'.$image.'.jpg"';?>/>
		</div>
		<input type="file" name="file" id="file" />
		<textarea id="postText" name="postText" cols="" rows="3" placeholder="Description (optional)"><?=safeValue($text)?></textarea>
	</div>

	<input type="hidden" name="lat" value="<?=safeValue($lat)?>">
	<input type="hidden" name="lng" value="<?=safeValue($lng)?>">
	<input type="hidden" name="mapId" value="<?=safeValue($mapId)?>">
	<input type="hidden" name="id" value="<?=safeValue($id)?>">

	<div id="<?=safeValue($id) ? 'update-post-btn':'continue-btn' ?>"><?=safeValue($id) ? 'Update':'Continue' ?></div> 
</form>

