<div class="popup" id="info"></div>
<div class="popup" id="searchWindow"></div>
<div class="popup" id="shareWindow">
    <div class="managementTitleBar">
        <h3 class="managementTitle">Share this to your favourite sites</h3>
        <div class="closeBtn"></div>
    </div>
    <div id="shareContent">
    
    </div>
</div>
<div class="popup" id="requestMap">
    <div class="managementTitleBar">
        <h3 class="managementTitle">Want a map of your very own? Request early access</h3>
        <div class="closeBtn"></div>
    </div>
    <form class="form-container" id="requestForm" name="requestForm">
        <input id="form-text-box" type="text" name="Email Form"  placeholder="Your Email" required>
        <input id="requestSub" class="button" type="submit" value="Request Map">
    </form>
</div>
<div class="popup" id="emailForm"></div>