<menu id="management">
    <div class="managementBtn tooltip" data-tip="Create new map" id="createMap"></div>
    <div class="managementBtn tooltip" data-tip="List maps you own" id="myMaps"></div>
    <div class="managementBtn tooltip" data-tip="List posts you've made" id="myPosts"></div>
    <div class="managementBtn tooltip" data-tip="List your subscriptions" id="mySubscriptions"></div>
    <div class="managementBtn tooltip" data-tip="View management options" id="manage"></div>
</menu>
    <div class="managementBtn tooltip" data-tip="mass upload" id="massUpload"></div>
