<section id="comments">
	<div class="managementTitleBar">
		<h2 class="managementTitle">Comments</h2>
		<div class="closeBtn"></div>
	</div>
	<div id="commentsBody">
		<form id="newComment">
			<textarea name="comment" id="comment"></textarea><br>
			<input class="button" type="submit" name="postComment" id="postComment" value="POST">
		</form>
<?php
	if (!empty($comments))
		foreach($comments as $key=>$value)
			echo '<article class="comment">
				<h4 class="commentUsername">'.$value['userName'].'</h4>
				<time class="commentDate" datetime="'.gmdate("Y-m-d\TH:i:s\Z", $value['modified']).'">'.gmdate('D, M j g:i a', $value['modified']).'</time>
				<pre class="commentText">'.$value['content'].'</pre>
			</article>';
?>
	</div>
</section>