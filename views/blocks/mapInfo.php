<section class="rightSlideIn" id="mapInfo">
    <div class="managementTitleBar">
        <h2 class="managementTitle"><?=$name?></h2>
        <div class="closeBtn"></div>
    </div>
    <div id="mapInfoBody">
        <menu class="standard">
            <?php if(!empty($lgd)):?>
            <div class="iconBtn standard subscribe submap_<?=$id?><?php echo !safeValue($del)?' active':'';?>" data-id="<?=safeValue($id)?>"><?php echo !safeValue($del)?'Subscribed':'Subscribe';?></div><span><?=safeValue($subCount)?></span>
            <?php endif;?>
            <div class="iconBtn standard share shareMap tooltip" data-tip="Share this map" data-name="<?=safeValue($name)?>"></div>    
        </menu>
        <?php if ($image):?>
            <img src="/uploads/images/<?=safeValue($image)?>">
        <?php endif;?>

        <h4>About</h4><br><br>
        <?php if(!empty($created)):?><time datetime="<?=gmdate('Y-m-d\TH:i:s\Z', $created)?>">Created <?=gmdate('D, M j g:i a', $created)?></time><?php endif;?>
        <pre id="mapDesc"><?=$description?></pre>
    </div>
</section>