<!DOCTYPE html>
<html>
	<head>

		<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
		<meta name="description" content="<?=$mapDesc?>">
		<meta name="author" content="<?=safeValue($author)?$author:'Snaplas Inc'?>">
		<meta name="viewport" content="minimal-ui, width=device-width, height=device-height, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
		<meta name="apple-mobile-web-app-capable" content="yes" />
		<meta property="og:title" content="<?=safeValue($postName).$mapName?> | Snaplas" /> 
		<title><?=safeValue($postName).$mapName?> | Snaplas</title>

		<?= loadCSS($css)?>		

		<!--<link rel="stylesheet" href=".<?=JS_DIR?>leaflet/leaflet.css" />-->
		<?php if ($image):?>
			<link rel="image_src" href="/uploads/images/<?=$image?>" />
			<meta property="og:image" content="/uploads/images/<?=$image?>" /> 
		<?php else:?>
			<link rel="image_src" href="/images/icon.png" />
			<meta property="og:image" content="/images/icon.png" /> 

		<?php endif;?>

		
		<script type="text/javascript">
			<?php if(isset($pId)):?>
			var pId = <?=$pId?>;
			var lat = <?=$lat?>;
			var lng = <?=$lng?>;
			<?php endif;?>
			
			var locate = <?=$locate?>;
			var mapId = [<?=$mapId?>];
			var startZoom = <?=$startZoom?>;
			var startLat = <?=$startLat?>;
			var startLng = <?=$startLng?>;
		</script>


		<?=$markerTemplate?>
		<?=$mapPreviewTemplate?>
		<?=$postPreviewTemplate?>

		<!--<script type="text/javascript" src=".<?=JS_DIR?>prefixfree.min.js"></script>-->
		<!--<script src=".<?=JS_DIR?>leaflet/leaflet.js"></script>-->


	</head>
	<body>
		<div id="fb-root"></div>
		<script>(function(d, s, id) {
		  var js, fjs = d.getElementsByTagName(s)[0];
		  if (d.getElementById(id)) return;
		  js = d.createElement(s); js.id = id;
		  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&appId=1388310581457148&version=v2.0";
		  fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));</script>	
		<?=safeValue($mapbar)?>
		<header id="mainHeader">
			<div id="logo" class="tooltip" data-tip="Go back to home page">
				<a href="/">
					<img src="/images/snaplas-logo.svg" title="Snaplas" alt="Snaplas" />
				</a>
			</div>
			<!--<div id="headerMarker"></div>-->
			<form id="searchbar" style="">
				<input type="text" name="search" placeholder="Search by location" id="search" />
				<button id="searchBtn" class="tooltip" data-tip="Search for a location" type="submit" value=""></button>
			</form>
      <?php
        if ($canPost && (isset($_SESSION['login']) && $_SESSION['login'] == "success"))
          echo '<menu id="mapMenu"><div id="photoBtn" class="tooltip" data-tip="Post to map"></div></menu>';
      ?>
			<!--<?php if(isset($_SESSION['login']) & $_SESSION['login'] === 'success')?>-->
			<!--<menu id="sortMenu">
				<a href="#" class="navLink" id="rated">High Rank</a>
				<a href="#" class="navLink" id="newest">Newest</a>
				<a href="#" class="navLink" id="oldest">Oldest</a>
			</menu>-->

			<span id="photoText">
				Click the map to upload an image
			</span>
				<!--<input type="radio" class="select" name="sort" id="rank" /><label for="rank">R</label> <span style="color:white;">/ </span.<input type="radio" class="select" name="sort" id="new" /><label for="new">N</label>
			<a href="#" style="color:white; margin-left:20px;" onclick="">
				<div style="display:inline-block; width:60px; height:30px; background:url('./images/icons.png') 0 -335px; background-size:210%;"></div>
			</a>-->


			<?=$loginBtn?>
			<div id='profileBox'></div>

			<?=$managementBar?>			
			<div id="mapMenuBtn" class="tooltip" data-tip="View map info"><?=$mapName?></div>
		</header>


		<!--<body>-->
		<menu id="menu" style="display:none;">
			<ul>
				<li>Upload</li>
				<li>Gallery</li>
				<li>Share</li>
			</ul>
		</menu>
		<?php
			//may not need this anymore for snaplas
			
			/*require ("./galleryClass.php");
			$gal = new gallery();
			$gal->setGalleryDirectory("./uploads");
			$gal->makeGallery();
			$gal->displayGallery(0);*/
		?>
		
		<div id="map"></div>
		<?=safeValue($mapInfo)?>
		<?=safeValue($postManager)?>
		<?=safeValue($subscriptionsManager)?>
		<?=safeValue($mapsManager)?>		

		<footer id="footer">
	    		<section id="connect">
				<a href="/"><span id="copyright">© 2014 snaplas.com</span></a>
				<a href="#about" class="aboutBtn">About</a>
				<a href="#privacy" class="privacyBtn">Privacy</a>
				<a href="#terms" class="termsBtn">Terms</a>
				<a href="https://twitter.com/Snaplas" id="twtr" target="_blank"></a>
				<a href="https://www.facebook.com/snaplas?fref=ts" id="fb" target="_blank"></a>
				<a href="mailto:info@snaplas.com" id="eml"></a>	
	    		</section>
		</footer>
		
		<?=$postTemplate?>
		<?=$comments?>
		
		<?=$popups?>
		<div id="links"></div>
		<?php include "analytics.php"?>
		<?= loadJS($js)	?>		
	</body>
</html>
