function postsObj(){
    
    var obj = this;
	this.activePost = {};
	this.page = 0;
    
    document.querySelector("#showPhotoDesc").addEventListener("click", function(){
		$("#showPhotoDesc").toggleClass("active");
		$("#photoDesc").slideToggle("slow");
	});
	

	$("#bottomPanel .settings").on('click', function(e){
		e.stopPropagation();
		$('.postMenu').slideUp('fast');
		$(this).children('.postMenu').first().stop().slideToggle('fast');
	});
	


	//toggle comments view
	$(".iconBtn.commentBtn").on("click", function(e){
		e.preventDefault();
		if ($("#comments").hasClass("open"))
			$("#comments").css({bottom:"-"+$("#comments").height()+"px"}).removeClass("open");
		else
		{
			obj.getComments(obj.activePost.imgId);
			//$(".comment").remove();
		}
		$(this).toggleClass("active");
	})

	$("#comments").on("click", ".closeBtn", function(e){
		e.stopPropagation();
		//close side section
		$section = $(this).parents("section");
		$section.css({bottom:"-"+$("#comments").height()+"px"}).removeClass("open");
		$(".iconBtn.commentBtn").removeClass("active");
	})
	
	
	$(".postMenu").menu({
		select:function(e, elm){

			if (elm.item.hasClass('editBtn'))
			{
				getPostFunc(elm.item.attr("data-id"));
			}
			else if (elm.item.hasClass('deleteBtn'))
			{
				$.post('/posts/deletePost',	{id:elm.item.attr("data-id")}, function(data){
					if (data.status == "suc")
					{
						elm.item.parents('.postPreview').remove();
						snapmap.scanMap(snapmap.map);
					}
				}, "json");								
			}
			else if (elm.item.hasClass('reportBtn'))
			{
				//getPostFunc(elm.item.attr("data-id"));
			}
		}
	}).hide();	
	
	$("#newComment").on("submit", function(e){
		e.preventDefault();
		obj.postComment(obj.activePost.imgId, $("#comment").val());
	})
}

postsObj.prototype.setActive = function(_active)
{
	var obj = this;
	obj.activePost = _active;
}

postsObj.prototype.viewPost = function(_imageId, _mapId, _liked, _callback)
{
		var obj = this;		

		$.get("/maps/getPost", {pid:_imageId, mapId:_mapId, liked:_liked}, function(data){
			if (data.status == "suc")
			{
				/*if(data.data.liked)
					document.querySelector(".iconBtn#likes").classList.add("likeOn");
				else
					document.querySelector(".iconBtn#likes").classList.remove("likeOn");*/

				if (obj.activePost.imgId != _imageId)
				{
					obj.activePost = {
						"markerLocation": {
							"lat":parseFloat(data.data.lat), "long":parseFloat(data.data.lng)
						},
						"user": data.data.userName,
						"rank": data.data.rank,
						"img": data.data.image+"",
						"imgId": data.data.id,
						"title": data.data.title,
						"other": "none",
						"from":"canada",
						"open":false
					}
				}

				document.querySelector("#bottomPanel .panelTop .mapTitle a").href = "/m/"+data.data.mapName.split(' ').join('-');
				document.querySelector("#bottomPanel .panelTop .mapTitle a").innerHTML = data.data.mapName;				
				document.querySelector("#bottomPanel .panelTop .postUser").innerHTML = "by "+data.data.userName;
				//document.querySelector("#likeCount").innerHTML = data.data.num;
				document.querySelector("#bottomPanel .panelTop .panelTitle").innerHTML = data.data.title;
				document.querySelector("#bottomPanel .panelTop .postDate").innerHTML = moment(data.data.uploadDate, "X").format("llll");
				document.querySelector("#bottomPanel .panelTop .postDate").datetime = moment(data.data.uploadDate, "X").format("YYYY-MM-DDTHH:mm:SSZ");
				document.querySelector("#commentBtn").href =  window.location.pathname.replace(/\/[0-9]+$/, "").replace(/\/$/, "")+"/"+_imageId;

				//hide or show relivant settings menu items
				if (data.data['owner'])
				{
					$("#bottomPanel .panelTop .editBtn").attr("data-id", data.data.id).show();
					$("#bottomPanel .panelTop .deleteBtn").attr("data-id", data.data.id).show();
					$("#bottomPanel .panelTop .reportBtn").attr("data-id", '').hide();
					$(".postMenu").menu("refresh");
				}
				else
				{
					$("#bottomPanel .panelTop .editBtn").attr("data-id", '').hide();
					$("#bottomPanel .panelTop .deleteBtn").attr("data-id", '').hide();
					$("#bottomPanel .panelTop .reportBtn").attr("data-id", data.data.id).show();
					$(".postMenu").menu("refresh");
				}
				
				if (data.data.text.length)
				{
					$("#photoDesc").hide().html(linkify(data.data.text));
					$("#showPhotoDesc").show();	
				}
				else
				{
					$("#photoDesc").hide().text("");
					$("#showPhotoDesc").hide();
				}
				document.querySelector("#postContent").innerHTML = '<img src="/uploads/images/'+data.data.image+'.jpg">';//.css({'bottom':($('#photoDesc').height()+15)+"px"});
				$("#bottomPanel").fadeIn(200).attr("data-id", _imageId);		    
				if (_callback)
					_callback.call(snapmap, obj.activePost.imgId, obj.activePost.markerLocation.lat, obj.activePost.markerLocation.long);
				return true;
			}
			else
			{
				alert(data.msg);
			}
		}, "json");

}


postsObj.prototype.likeImage = function(_imageId)
{
    var obj = this;
    snapmap.checkLogin();
    $.post("/functions", {"imgId":_imageId, "func":"setRank"}, function(data){
        if(!data.liked)
        {
            $(".sideBtn#likes").addClass("likeOn");
            $("#likeCount").text(parseInt(data.num)+1);
        }
        else
        {
            $(".sideBtn#likes").removeClass("likeOn");
            $("#likeCount").text(parseInt(data.num)-1);
        }
    }, "json")
}

postsObj.prototype.postComment = function(_postId, _comment)
{
	$.post("/comments/postComment", {"postId":_postId, "comment":_comment}, function(data){
		if (data.status === "suc")
		{
			var frag = '<article class="comment">'+
				'<h4 class="commentUsername">You</h4>'+
				'<time class="commentDate" datetime="">Just Now</time>'+
				'<pre class="commentText">'+_comment+'</pre>'+
			'</article>';
			$("#newComment").after(frag);
			$("#comment").val("")
		}
		else
			alert(data.msg);
	}, "json")
}

postsObj.prototype.getComments = function(_postId)
{
	$(".comment").remove();	
	$.get("/comments/getComments", {postId:_postId}, function(data){
		if (data.status === "suc")
		{
			for (x in data.data)
			{
				var frag = '<article class="comment">'+
							'<h4 class="commentUsername">'+data.data[x].userName+'</h4>'+
							'<time class="commentDate" datetime="'+moment(data.data[x].modified, "X").format("YYYY-MM-DDTHH:mm:SSZ")+'">'+moment(data.data[x].modified, "X").fromNow()+'</time>'+
							'<pre class="commentText">'+data.data[x].content+'</pre>'+
						'</article>';
				$("#commentsBody").append(frag);
			}
		}
		//else
		//	alert(data.msg);
	}, "json");
	$("#comments").addClass("open");
	
}