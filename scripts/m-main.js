
//create function
function mapsclick() { 
	$("#post-new").hide(); //on click hides #post-new
	$("#map-container").hide();	
	$listcontainer.show().addClass("map-list-container"); // in listcontainer swaps post-list-container for map-list-container
	$listcontainer.removeClass("post-list-container"); //above
	$listcontainer.html(""); // clears html to blank to put in map elements
	$("#other-container").hide();

	$.get("/users/getMaps", function(data){
		if (data.status == "suc")
			listMaps(data.data);
		else
			alert(data.msg)
	}, "json");	
	
}

function subsclick() { 
	$("#post-new").hide(); //on click hides #post-new
	$("#map-container").hide();	
	$listcontainer.show().addClass("map-list-container"); // in listcontainer swaps post-list-container for map-list-container
	$listcontainer.removeClass("post-list-container"); //above
	$listcontainer.html(""); // clears html to blank to put in map elements
	$("#other-container").hide();

	$.get("/users/getSubscriptions", {type:"public"}, function(data){
		if (data.status == "suc")
			listMaps(data.data);
		else
			alert(data.msg)
	}, "json");	
	
}

function postsclick(_mapId) {
	$("#post-new").show(); //on click shows #post-new
	$("#map-container").hide();		
	$listcontainer.show().removeClass("map-list-container"); // in listcontainer swaps map-list-container for post-list-container
	$listcontainer.addClass("post-list-container"); //above
	$listcontainer.html(""); // clears html to blank
	$("#other-container").hide();	
	$.get("/users/getPosts", {'mapId':_mapId}, function(data){
		if (data.status == "suc")
			listPosts(data.data);
		else if (data.status == "lgd")
			window.location = "/mobileupload";
	}, "json");		

}

function listMaps(_maps)
{
	for (var i = 0, j = _maps.length; i<j; i++){ // creates a loop that runs 5 times and gives list element a nr
		$listcontainer.append(maplisttemplate);  //maplisttemplate is appended to listcontainer
		$(".map-item-box:last-child .map-name").text(_maps[i].name); //in map-item-box change map-name to "#" and add element nr
		$(".map-item-box:last-child").attr("data-id",_maps[i].id);
		$(".map-item-box:last-child .map-subscribers").text(_maps[i].subCount);
		$(".map-item-box:last-child .map-creation-date").attr("datetime",moment(_maps[i].created, "X").format("YYYY-MM-DDTHH:mm:SSZ")).text(moment(_maps[i].created, "X").format("lll"));
	}	
}


function listPosts(_posts)
{
	for (var i = 0, j = _posts.length; i<j; i++){ // creates a loop that runs 5 times and gives list element a nr
		$listcontainer.append(postlisttemplate); //postlisttemplate is appended to listcontainer(why?)
		$(".post-item-box:last-child .post-info-name").text(_posts[i].title); //in post-item-box change post-info-name to "#" and add element nr
		$(".post-item-box:last-child").attr("data-id",_posts[i].id); 
		$(".post-item-box:last-child .post-image").html('<img src="/uploads/thumbs/'+_posts[i].image+'.jpg">'); 
		
		$(".post-item-box:last-child .post-info-date").attr("datetime",moment(_posts[i].modified, "X").format("YYYY-MM-DDTHH:mm:SSZ")).text(moment(_posts[i].modified, "X").format("lll"));
	}	
	
}

function getPostFunc(_id)
{
	$listcontainer.hide(); // clears html to blank	
	$("#post-new").hide(); //on click hides #post-new
	$.get('/posts/upload_file',	{id:_id}, function(data){
		$("#other-container").html(data).show();
		if (_id)
			$("input[name=mapId]").val(_id);
		else
			$("input[name=mapId]").val(mapId);
	});
}


function postToMap(_postData)
{
	$("#post-a-post-btn").text("Posting...");
	$.ajax({
		url: '/posts/postToMap',
		data: _postData,
		processData: false,
		contentType: false,
		dataType:'json',
		type: 'POST',
		success: function(data){
			if (data.status == "suc")
				postsclick();
			else
				alert(data.msg);
				$("#post-a-post-btn").text("Posting...");
		}
	}); 	
}


function makeDataUrl(input) {
	
	if (input.files && input.files[0]) {
		if (!input.files[0].type.match('image/jpeg') &&
			!input.files[0].type.match('image/png') &&
			!input.files[0].type.match('image/gif'))
		{
			alert("image must be .jpg, .png, or .gif format");
			$("#file").val("");
		}
		var reader = new FileReader();
		
		reader.onload = function (e) {
			$('#preview').attr('src', e.target.result);
		}
		
		reader.readAsDataURL(input.files[0]);
	}
}


var $listcontainer=$("#list-container"); //defining $listcontainer (jquery obj.) as #list-container
var mapId=0; //defining mapid (name) as 0
var mapName = "";


$(".nav-btn").on("click", function(){ //takes nav-btn, adds a click function
	$(".nav-btn").removeClass("active"); //initialy removes active class from nav-btn
	$(this).addClass("active"); // adds active class to active element.
});

	
$("#nav-btn-maps").on( "click", function(){mapsclick()});
$("#nav-btn-subs").on( "click", function(){subsclick()});
$("#nav-btn-posts").on("click", function(){postsclick()});

$("#list-container").on("click", ".map-item-box", function(){
	mapId=$(this).attr("data-id");
	mapName = $(".map-name", this).text();
	postsclick(mapId);
	$(".nav-btn").removeClass("active"); //initialy removes active class from nav-btn
	$("#nav-btn-posts").addClass("active"); // adds active class to active element.	

}).on("click", ".post-item-box", function(){
	getPostFunc($(this).attr("data-id"));
});

$("#post-new").on("click", function(){
	if (mapId)
		getPostFunc(mapId);
	else
		alert("Please select a map!")
})

$("#main-box").on("click", "#post-a-post-image-upload", function(){
	$("#file").trigger("click");
	
})
$("#main-box").on("click", "#continue-btn", function(){
	if (!$("#file").val())
		alert("please attach an image to your post");
	else
	{
		$("#other-container").hide();
		$.get("/mobileupload/map", function(data){
			$("#map-container").html(data).show();
			$("#mapName").text(mapName);
		})
	}
})
$("#main-box").on("click", "#post-a-post-btn, #update-post-btn", function(){

	$("#post-a-post-form-size").trigger("submit");
		
})
$("#main-box").on("submit", "#post-a-post-form-size", function(e){
	e.preventDefault();
	var formData = new FormData($(e.target)[0]);
	postToMap(formData);

})
$("#main-box").on("change", "#file", function(){
	makeDataUrl(this);
});


listMaps(maps);