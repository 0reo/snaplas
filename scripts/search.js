searchResults = [];
function geocode(searchStr, $div)
	{
	$.support.cors = true;
		$.ajax({
			'url':'//nominatim.openstreetmap.org/search?q='+searchStr+'&format=json&bounded=1',
			'crossDomain':true,
			'async':false,
			'type':'get',
			'dataType':'json',
			'success':function (data){
			        searchResults = [];
				if (data.length > 0)
				{
					console.log($div.children(".topBar"));
					if (data.length >= 1)
						$("#searchTopBar").prepend('Search Results:'); //$('#content')
					searchResults = data;
					for (var key = 0; key < data.length; key++)
					{
						var itemstr = '';
					            for (key2 in data[key])
					            {
					            	if (key2 === 'display_name')
	  							itemstr += '<div class="result" onclick=\'sLat = '+data[key]['lat']+
        						        '; sLng = '+data[key]['lon']+
						                '; $(".closeBtn").click();\' id='+
						                data[key]['place_id']+
						                '>' +(key +1)+' - '+ data[key][key2]+
						                '</div></span>';
					            }
						$div.append(itemstr);
					}
				}
				else
					$("#searchTopBar").prepend('Sorry, your search returned no results.');
			},
			'error':function(data, status, errNo){
				console.log(errNo);
				alert('error - '+status);
			}
		});
	}

$(document).on("click", ".result", function(){
  var i = $(this).index(".result");
  //console.log(searchResults[i]);
  //fLat = sLat; fLng = sLng;
  //snapmap.moveMap(fLat, fLng);

  var southWest = new L.LatLng(searchResults[i].boundingbox[1], searchResults[i].boundingbox[3]),
    northEast = new L.LatLng(searchResults[i].boundingbox[0], searchResults[i].boundingbox[2]),
    bounds = new L.LatLngBounds(southWest, northEast);

  snapmap.moveMap(bounds);
});