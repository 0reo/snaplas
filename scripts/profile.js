function userManagementObj() {
	
	this.pStr = "#profileBox";
	this.btn = "";
	this.mapPreviewTemplate = null;
	this.postPreviewTemplate = null;

	//error handlers.  may not need anymore due to html5 error handling
	this.errors = {
		"password2":{
			"msg":"Passwords much match",
			"set":false
		}
	};
	this.profile = {};
	
	var obj = this;


}

userManagementObj.prototype.init = function(lgd)
{
	var obj = this;
	lgd? obj.btn = "#updateProfile" : obj.btn = "#registerBtn";	

	obj.mapPreviewTemplate = $('#mapPreviewTemplate').html();
	obj.postPreviewTemplate = $('#postPreviewTemplate').html();
	Mustache.parse(obj.mapPreviewTemplate);
	Mustache.parse(obj.postPreviewTemplate);

	$('#postPreviews').perfectScrollbar({suppressScrollX:true});
	$('#subscriptions').perfectScrollbar({suppressScrollX:true});
	$('#ownedMaps').perfectScrollbar({suppressScrollX:true});
	
	//handles user registration/profile update
	$("#mainHeader").on("click", "#registerBtn", function(e){

		var username = $("#username").val();
		var password = $("#password").val();

		$.get("/users/profile", function(data){
			$(obj.pStr).html(data).show("slow").promise().done(function(){
				if (username !== undefined && username.length)
					$("#usernamePro").val(username).trigger("change");
				if (password !== undefined && password.length)
					$("#epassword").val(password).trigger("change");			

				obj.handleErrors();
			});
		});		

	});

	//submit profile
	$(obj.pStr).on("submit", "#profileForm", function(e){
		e.preventDefault();

		for (x in obj.errors)
		{
			if (obj.errors[x].set)
			{
				console.log(obj.errors[x]);
				$('#message').text(obj.errors[x].msg);
				$('input[name="'+x+'"]').css('background', "yellow");
				return false;
			}
		}
	
		if (obj.btn == "#registerBtn")
		{
			obj.register();
		}
		else if(obj.btn == "#updateProfile")
		{
			obj.updateProfile();
		}
	});	
	
	
	//date picker for age
	$('body').on('focus',"#age", function(){
		$(this).datepicker({
			dateFormat : 'yy-mm-dd',
			changeMonth : true,
			changeYear : true,
			yearRange: '-99:-18',
			maxDate: '-18y',
			minDate: '-100y'
		});
	});


			
	$(".managementBtn").on("click", function(e){	
		if(e.target.id == "manage" && !$(this).hasClass("active"))	//open management interface
		{
			$("#manage").addClass("active");
			$('#management').css({'width':'220px', 'border-radius':"5px 20px 20px 5px", "overflow":"hidden"});
		}
		else if(e.target.id == "manage" && $(this).hasClass("active"))  //close management interface
		{
			$('#management').css({'width':'40px', 'border-radius':"20px 20px 20px 20px", "overflow":"hidden"});
			$(".managementBtn").removeClass("active");
			$(".rightSlideIn").removeClass("open");
		}
		else if(e.target.id == "createMap" && !$(this).hasClass("active"))
		{
			$(".rightSlideIn").removeClass("open");			
			$(".managementBtn:not(#manage)").removeClass("active");
			$(this).addClass("active");

			$("#requestMap").fadeIn(200);			

		}	
		else if(e.target.id == "myPosts" && !$(this).hasClass("active"))
		{
			$(".rightSlideIn").removeClass("open");			
			$(".managementBtn:not(#manage)").removeClass("active");
			$(this).addClass("active");
			obj.getPosts();
			
		}
		else if(e.target.id == "mySubscriptions" && !$(this).hasClass("active"))
		{
			$(".rightSlideIn").removeClass("open");			
			$(".managementBtn:not(#manage)").removeClass("active");
			$(this).addClass("active");
			obj.getSubscriptions();
		}
		else if(e.target.id == "myMaps" && !$(this).hasClass("active"))
		{
			$(".rightSlideIn").removeClass("open");			
			$(".managementBtn:not(#manage)").removeClass("active");
			$(this).addClass("active");
			obj.getMaps();
		}
		else if(e.target.id == "massUpload" && !$(this).hasClass("active"))
		{

				$.get('/posts/massUpload',
				{
					mapId:snapmap.mapId
				}, function(data){
					$("#upload").html(data).fadeIn(200);
				});

			snapmap.hoverOutImage(e, posts.activePost, false);
			$("#bottomPanel").fadeOut(200).removeAttr("data-id");
			history.replaceState('data', '', window.location.pathname);			
			ga('send', 'pageview', window.location.pathname.replace(/\/[0-9]+$/, "").replace(/\/$/, "") );	///google analytics
			$("#photoDesc").text("");
			$("#comments .closeBtn").trigger("click");
		}
		else
		{
			$(".managementBtn:not(#manage)").removeClass("active");
			$(".rightSlideIn").removeClass("open");
		}
		
	}).on("mouseover", function(){
		$('#management').css({"overflow":"visible"});
	}).on("mouseout", function(){
		$('#management').css({"overflow":"hidden"});
	})





	$(document).on('click', '.settings', function(e){
		e.stopPropagation();
		$('.editPost').slideUp('fast');
		$(this).children('.editPost').first().stop().slideToggle('fast');
	}).on('click', '.postLink', function(){
		posts.viewPost($(this).attr("data-id"), 0, true);			
	}).on('click', function(){
		$('.settingsMenu').slideUp('fast');
	}).on("submit", "#requestForm", function(e){
		e.preventDefault();
		obj.requestMap();		
	});
	
	
	$(".editPost").menu({
		select:function(e, elm){

			if (elm.item.hasClass('editBtn'))
			{
				getPostFunc(elm.item.attr("data-id"));
			}
			else if (elm.item.hasClass('deleteBtn'))
			{
				$.post('/posts/deletePost',	{id:elm.item.attr("data-id")}, function(data){
					if (data.status == "suc")
					{
						elm.item.parents('.postPreview').remove();
						snapmap.scanMap(snapmap.map);
					}
				}, "json");								
			}
		}
	}).hide();
	
	
	$(document).on("click", ".multi", function(){
		
		
	})
}

function getPostFunc(_id)
{
	$.get('/posts/upload_file',	{id:_id}, function(data){
		$("#upload").html(data).toggle("slow");
		$("#changeMap").hide();

	});
}

userManagementObj.prototype.handleErrors = function()
{
	var obj = this;


	$(obj.pStr+' input').each(function(){
		$(this).on("change", function(){
			field = $(this).attr("name");
			if (field == "password2")
			{
				obj.errors[field].set = false;
				if ($(this).val().length && $(this).val() !== $("#epassword").val())
				{
					$('#message').text(obj.errors[field].msg);
					obj.errors[field].set = true;
					$(this).css('background', "yellow");
				}
				else
				{
					$(this).css('background', "");
					$('#message').text("");
					obj.errors[field].set = false;
				}
			}
		});
	});	
}

userManagementObj.prototype.register = function()
{
	var obj = this;

	obj.profile["register"] = true;
	$(obj.pStr+" input").each(function(){
		if ($(this).attr("name")!== undefined && $(this).val().length)
			obj.profile[$(this).attr("name")] = $(this).val();
	})
	obj.profile["country"] = $("#country").val();
	$.post("/users/register", obj.profile, function(data){
		if (data.status == "suc")
			window.location = "";
		else
			alert(data.msg);
	}, 'json')	
}

userManagementObj.prototype.updateProfile = function()
{
	var obj = this;

	$(obj.pStr+" input").each(function(){
		if ($(this).attr("name")!== undefined && $(this).val().length)
			obj.profile[$(this).attr("name")] = $(this).val();
	})
	obj.profile["country"] = $("#country").val();
	delete obj.profile["password2"];
	obj.profile["update"] = true;
	$(obj.pStr+" input").each(function(){
		if ($(this).attr("name")!== undefined && $(this).val().length)
			obj.profile[$(this).attr("name")] = $(this).val();
	})
	obj.profile["country"] = $("#country").val();
	$.post("/users/updateProfile", obj.profile, function(data){
		if (data.status == "suc")
		{
			$("#opacity").hide();
			$(obj.pStr).hide("slow").promise().done(function(){
				$(obj.pStr).html('');
			});
			obj.profile = {};
		}
		else
		{
			alert(data.msg);
		}
	}, 'json')	
}

userManagementObj.prototype.getPosts = function()
{
	var obj = this;
	$.get("/users/getPosts", function(data){
		if (data.status == "suc")
		{
			$(".postPreviews").remove();
			for (x in data.data)
			{
				//console.log(data.data[x]);
				
				var frag = Mustache.render(obj.postPreviewTemplate, {
					previewType:"postPreview",
					postTitle:data.data[x].title,					
					mapName:data.data[x].mapName,
					mapNameUrl:data.data[x].mapName.split(' ').join('-'),
					dateTime:moment(data.data[x].uploadDate, "X").format("YYYY-MM-DDTHH:mm:SSZ"),
					created:moment(data.data[x].uploadDate, "X").format("lll"),
					image:data.data[x].image+".jpg",
					postId:data.data[x].id
				});
											
				$("#postPreviews").append(frag);
			}
			$(".editPost").menu({
				select:function(e, elm){

					if (elm.item.hasClass('editBtn'))
					{
						getPostFunc(elm.item.attr("data-id"));
					}
					else if (elm.item.hasClass('deleteBtn'))
					{
						$.post('/posts/deletePost',	{id:elm.item.attr("data-id")}, function(data){
							if (data.status == "suc")
							{
								elm.item.parents('.postPreview').remove();
								snapmap.scanMap(snapmap.map);
							}
						}, "json");								
					}
				}
			}).hide();
			$("#managePost").addClass("open");
		}
	}, 'json');	
}


userManagementObj.prototype.getSubscriptions = function()
{
	var obj = this;
	$.get("/users/getSubscriptions", function(data){
		if (data.status == "suc")
		{
			$(".subscriptionPreview").remove();
			for (x in data.data)
			{
				//console.log(data.data[x]);
				
				var frag = Mustache.render(obj.mapPreviewTemplate, {
					previewType:"subscriptionPreview",
					hoverType:"active",
					mapName:data.data[x].name,
					mapNameUrl:data.data[x].name.split(' ').join('-'),
					dateTime:moment(data.data[x].created, "X").format("YYYY-MM-DDTHH:mm:SSZ"),
					created:moment(data.data[x].created, "X").format("lll"),
					image:data.data[x].image,
					mapId:data.data[x].id,
					subCount:data.data[x].subCount,
					subscription:true
				});

				$("#subscriptions").append(frag);
			}
			$("#manageSubscriptions").addClass("open");
		}
		else if(data.status == "wrn")
		{
			alert(data.msg);
		}		
	}, 'json');	
}

userManagementObj.prototype.getMaps = function()
{
	var obj = this;
	$.get("/users/getMaps", function(data){
		if (data.status == "suc")
		{
			$(".mapPreview").remove();
			for (x in data.data)
			{
				//console.log(data.data[x]);

				var frag = Mustache.render(obj.mapPreviewTemplate, {
					previewType:"mapPreview",
					hoverType:"static",
					mapName:data.data[x].name,
					mapNameUrl:data.data[x].name.split(' ').join('-'),
					dateTime:moment(data.data[x].created, "X").format("YYYY-MM-DDTHH:mm:SSZ"),
					created:moment(data.data[x].created, "X").format("lll"),
					image:data.data[x].image,
					mapId:data.data[x].id,
					subCount:data.data[x].subCount
				});

				$("#ownedMaps").append(frag);
			}
			$("#manageMaps").addClass("open");
		}
		else// if(data.status == "wrn")
		{
			alert(data.msg);
		}		
	}, 'json');	
}

userManagementObj.prototype.requestMap = function()
{
	profile["register"] = true;
	$("#profileBox input").each(function(){
		if ($(this).attr("name")!== undefined && $(this).val().length)
			profile[$(this).attr("name")] = $(this).val();
	})

	$.post("/users/requestMap", {"email":$("#form-text-box").val()}, function(data){
			alert(data.msg);
	}, 'json')	
}