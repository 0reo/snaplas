	var profile = {};
	var openBox = false;
	var errors = {
		"password2":{
			"msg":"Passwords much match",
			"set":false
		}
	};

	$('body').on('focus',"#age", function(){
		$(this).datepicker({
			dateFormat : 'yy-mm-dd',
			changeMonth : true,
			changeYear : true,
			yearRange: '-99:-18',
			maxDate: '-18y',
			minDate: '-100y'
		});
	});


	//map menu click events
	$( "#header-container" ).on( "click",".maps-btn",function() {
		$( ".maps-menu-closed" ).toggleClass( "maps-menu-open" );
	});

	$("#sign-in-btn").on("click", function(){
		if ($("#profileBox").html().length)
			$("#profileBox").hide('slow', function(){
				$(this).html("");
				openBox = false;
			});
		else
		{
			openBox = true;
			$("#profileBox").load("/login").toggle('100').promise().done(function(){
				//usermanagement.handleErrors();	
			});
			
		}
	})
	
	
		//handles user registration/profile update
	$(document).on("click", "#registerBtn, #registerBtnMain", function(e){

		if ($("#profileBox").html().length && e.target.id == "registerBtnMain")
			$("#profileBox").hide('slow', function(){
				$(this).html("");
				openBox = false;
			});
		else
		{
			openBox = true;
			var username = $("#username").val();
			var password = $("#password").val();

			$.get("/users/profile", function(data){
				$("#profileBox").html(data).show("slow").promise().done(function(){
					if (username !== undefined && username.length)
						$("#usernamePro").val(username).trigger("change");
					if (password !== undefined && password.length)
						$("#epassword").val(password).trigger("change");			
					handleErrors();
				});
			});		
		}

	}).on("click", "#splash-bg", function(e){
		e.stopPropagation();
		if (openBox && e.target.id !== "sign-in-btn" && e.target.id !== "registerBtn" && e.target.id !== "registerBtnMain")
			$("#profileBox").html("").hide("slow");
	});
	
	function handleErrors()
	{

		$('#profileBox input').each(function(){
			$(this).on("change", function(){
				field = $(this).attr("name");
				if (field == "password2")
				{
					errors[field].set = false;
					if ($(this).val().length && $(this).val() !== $("#epassword").val())
					{
						$('#message').text(errors[field].msg);
						errors[field].set = true;
						$(this).css('background', "yellow");
					}
					else
					{
						$(this).css('background', "");
						$('#message').text("");
						errors[field].set = false;
					}
				}
			});
		});	
	}


	//submit profile
	$(document).on("submit", "#profileForm", function(e){
		e.preventDefault();
		handleErrors();
		for (x in errors)
		{
			if (errors[x].set)
			{
				$('#message').text(errors[x].msg);
				$('input[name="'+x+'"]').css('background', "yellow");
				return false;
			}
		}
	
		lgd ?updateProfile():register();
		
	}).on("submit", "#requestForm", function(e){
		e.preventDefault();
		requestMap();		
	});	
	
	
	
	function register()
	{
		profile["register"] = true;
		$("#profileBox input").each(function(){
			if ($(this).attr("name")!== undefined && $(this).val().length)
				profile[$(this).attr("name")] = $(this).val();
		})
		profile["country"] = $("#country").val();
		$.post("/users/register", profile, function(data){
			if (data.status == "suc")
				window.location = "";
			else
				alert(data.msg);
		}, 'json')	
	}
	
	function requestMap()
	{
		profile["register"] = true;
		$("#profileBox input").each(function(){
			if ($(this).attr("name")!== undefined && $(this).val().length)
				profile[$(this).attr("name")] = $(this).val();
		})

		$.post("/users/requestMap", {"email":$("#form-text-box").val()}, function(data){
				alert(data.msg);
		}, 'json')	
	}	

	function updateProfile()
	{
		$("#profileBox input").each(function(){
			if ($(this).attr("name")!== undefined && $(this).val().length)
				profile[$(this).attr("name")] = $(this).val();
		})
		profile["country"] = $("#country").val();
		delete profile["password2"];
		profile["update"] = true;
		$("#profileBox input").each(function(){
			if ($(this).attr("name")!== undefined && $(this).val().length)
				profile[$(this).attr("name")] = $(this).val();
		})
		profile["country"] = $("#country").val();
		$.post("/users/updateProfile", profile, function(data){
			if (data.status == "suc")
			{
				$("#opacity").hide();
				$("#profileBox").hide("slow").promise().done(function(){
					$("#profileBox").html('');
				});
			}
			else
			{
				alert(data.msg);
			}
		}, 'json')	
	}
	
	
	
		var activePopup = '';
	
	$(document).on("click", ".aboutBtn", function(e){
		e.preventDefault();
		if (activePopup == "about")
			activePopup = '';
		else
			activePopup = "about";

		$.get("/about", function(data){
			$(".popup").fadeOut(200).promise().done(function(){
				if (activePopup == "about")
					$("#info").html(data).fadeIn(200);
			});
			
		});
		
	}).on("click", ".privacyBtn", function(e){
		e.preventDefault();
			if (activePopup == "privacy")
				activePopup = '';
			else
				activePopup = "privacy";

		$.get("/privacy", function(data){
			$(".popup").fadeOut(200).promise().done(function(){
				if (activePopup == "privacy")
					$("#info").html(data).fadeIn(200);
			});
		});
		
	}).on("click", ".termsBtn", function(e){
		e.preventDefault();
		if (activePopup == "terms")
			activePopup = '';
		else
			activePopup = "terms";

		$.get("/terms", function(data){
			$(".popup").fadeOut(200).promise().done(function(){
				if (activePopup == "terms")
					$("#info").html(data).fadeIn(200);
			});
		});
		
	}).on("click", ".closeBtn", function(){
		$(".popup").fadeOut(200);
	}).on("click", function(e){ //close projects menu if clicked off anywhere on document
		e.stopPropagation();
		$target=$(e.target);
		if($target.hasClass(".maps-menu-open")===false && $target.hasClass("maps-btn")===false){
			$( ".maps-menu-closed" ).removeClass( "maps-menu-open" );
		}
	});