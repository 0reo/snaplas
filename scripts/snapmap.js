	function snapmapObj()
	{
		this.map = null;
		this.clusters = null;
		this.mapId = 0;
		this.order = "rated";
		this.markerTemplate = null;
		this.marker = new Array();
		this.bounds = null;
		this.cla = 0.0;
		this.clo = 0.0;
		this.mrkImg = null;
		this.scannable = true;
		this.loggedin = false;
		this.mouseOver = false;
		this.locate = true;
		this.imgWidth = 115;
		this.imgHeight = 88;
		this.markerWidth = 115;
		this.uploadPhoto = false;
		this.hoverTimer = null;
	}

	snapmapObj.prototype.checkLogin = function()
	{
		var obj = this;
		if (!obj.loggedin)
		{
			alert("You must sign in to do that");
			return false;
		}
	}

	snapmapObj.prototype.init = function(_locate, _mapId)
	{
		var obj = this;
		obj.locate = _locate;
		obj.mapId = _mapId;

		obj.markerTemplate = $('#markerTemplate').html();	
		Mustache.parse(obj.markerTemplate);   // optional, speeds up future uses

		//var tileUrl = 'http://{s}.tile.cloudmade.com/BC9A493B41014CAABB98F0471D759707/997/256/{z}/{x}/{y}.png',
		var tileUrl = '//{s}.mqcdn.com/tiles/1.0.0/map/{z}/{x}/{y}.jpg',
		attr = '© OpenStreetMap contributors<br>Tiles Courtesy of <a href="http://www.mapquest.com/" target="_blank">MapQuest</a> <img src="//developer.mapquest.com/content/osm/mq_logo.png">',
		tileLayer = new L.TileLayer(tileUrl, {
			maxZoom: 18,
			subdomains: ["otile1-s","otile2-s","otile3-s","otile4-s"],
			attribution: attr
		});
		obj.clusters = new PruneClusterForLeaflet();
		PruneCluster.Cluster.ENABLE_MARKERS_LIST = true		;
		//obj.clusters.size = 1;
		obj.clusters.PrepareLeafletMarker = function(leafletMarker, data, category) {
			//leafletMarker.setIcon(/*... */); // See http://leafletjs.com/reference.html#icon
			//listeners can be applied to markers in this function


			if (data.icon) {
				if (typeof data.icon === 'function') {
					leafletMarker.setIcon(data.icon(data, category));
				} else {
					leafletMarker.setIcon(data.icon);
				}
			}

			if (leafletMarker.hasEventListeners("mouseover"))
				return true;

			leafletMarker.on('click', function(e){

				e.originalEvent.preventDefault();
				mid = obj.marker.indexOf($(this)[0]);
				obj.openPost(e, data);
		
			}).on("mouseover", function(e){
				e.originalEvent.stopPropagation();				

				if (e.originalEvent.target.className.baseVal !== "previewWindow")
					return false;	

				//obj.hoverTimer = setTimeout(function(){
					obj.hoverImage(e, data)
				//}, 500);
			
			}).on('mouseout', function(e){
				e.originalEvent.stopPropagation();
				if (e.originalEvent.target.className.baseVal !== "previewWindow")
					return false;
				//clearTimeout(obj.hoverTimer);

				//console.log("single out", e.originalEvent.target.nodeName);							
				obj.hoverOutImage(e, data, false)
			});

		};		

		obj.clusters.BuildLeafletClusterIcon = function(cluster) {
			var population = cluster.population, // the number of markers inside the cluster
				stats = cluster.stats; // if you have categories on your markers

			// If you want list of markers inside the cluster
			// (you must enable the option using PruneCluster.Cluster.ENABLE_MARKERS_LIST = true)
			var markers = cluster.GetClusterMarkers();
			var prevNum = Math.floor(Math.random() * markers.length);


			var rendered = Mustache.render(obj.markerTemplate, {
				markerId:cluster.hashCode,
				patternId:"patternCluster"+cluster.hashCode,
				imageUrl:markers[prevNum].data.img+".jpg",
				numMarkers:population
			});


			var MyIcon = L.divIcon({		
				iconSize: new L.Point('auto', 128),
				iconAnchor: new L.Point(64, 128),
				className:"customMarkOut",
				html:rendered
			});


			return MyIcon; 
		};

		obj.clusters.BuildLeafletCluster = function (cluster, position) {
        	var _this = this;
        	var m = new L.Marker(position, {
            	icon: this.BuildLeafletClusterIcon(cluster)
        	});

        	m.on('click', function () {
        		console.log("..");
				var markersArea = _this.Cluster.FindMarkersInArea(cluster.bounds);
				console.log(markersArea);
				var b = _this.Cluster.ComputeBounds(markersArea);

				if (b) {
					var bounds = new L.LatLngBounds(new L.LatLng(b.minLat, b.maxLng), new L.LatLng(b.maxLat, b.minLng));

					var zoomLevelBefore = _this._map.getZoom(), zoomLevelAfter = _this._map.getBoundsZoom(bounds, false, new L.Point(20, 20));

					if (zoomLevelAfter === zoomLevelBefore) {
						_this._map.fire('overlappingmarkers', {
							cluster: _this,
							markers: markersArea,
							center: m.getLatLng(),
							marker: m
						});

						_this._map.setView(position, zoomLevelAfter);
					} else {
						_this._map.fitBounds(bounds);
					}
				}
				
			/*}).on('dblclick', function () {
				var markersArea = _this.Cluster.FindMarkersInArea(cluster.bounds);
				var b = _this.Cluster.ComputeBounds(markersArea);

				if (b) {
					var bounds = new L.LatLngBounds(new L.LatLng(b.minLat, b.maxLng), new L.LatLng(b.maxLat, b.minLng));

					var zoomLevelBefore = _this._map.getZoom(), zoomLevelAfter = _this._map.getBoundsZoom(bounds, false, new L.Point(20, 20));

					if (zoomLevelAfter === zoomLevelBefore) {
						_this._map.fire('overlappingmarkers', {
							cluster: _this,
							markers: markersArea,
							center: m.getLatLng(),
							marker: m
						});

						_this._map.setView(position, zoomLevelAfter);
					} else {
						_this._map.fitBounds(bounds);
					}
				}
*/
			}).on("mouseover", function(e){
				e.originalEvent.stopPropagation();				

				if (e.originalEvent.target.className.baseVal !== "previewWindow")
					return false;	

				obj.mouseOver = true;
				//console.log(e.originalEvent.target.id);
				obj.hoverImage(e, e.originalEvent.target.parentNode)
				//console.log("cluster out", e);
			}).on('mouseout', function(e){
				e.originalEvent.stopPropagation();


				if (e.originalEvent.target.className.baseVal !== "previewWindow")
					return false;
				//clearTimeout(obj.hoverTimer);

				//console.log("single out", e.originalEvent.target.nodeName);							
				obj.hoverOutImage(e, e.originalEvent.target.parentNode, false)

			});
        	return m;
		}

		obj.map = new L.Map('map');
		obj.map.attributionControl.setPrefix('');
		obj.map.attributionControl.setPosition('bottomleft');
		//obj.map.setView(obj.getCurLatLng(), 12);
		obj.map.addLayer(tileLayer);
		obj.map.addLayer(obj.clusters);


		obj.map.on('load', function(e) {
		    obj.scanMap(obj.map);

		}).on('moveend', function(e) {
			if (!obj.mouseOver && obj.scannable)
				obj.scanMap(obj.map);
			obj.scannable = true;
			
		}).on('click', function(e){
			if (obj.uploadPhoto)
			{
				e.latlng;
				obj.uploadPhoto = false;
				$.get('/posts/upload_file',
				{
					lat:e.latlng.lat,
					lng:e.latlng.lng,
					mapId:obj.mapId
				}, function(data){
					$("#upload").html(data).fadeIn(200);
				});
			}
			obj.hoverOutImage(e, posts.activePost, false);
			$("#bottomPanel").fadeOut(200).removeAttr("data-id");
			//history.replaceState('data', '', window.location.pathname.replace(/\/[0-9]+$/, "").replace(/\/$/, ""));
			history.replaceState('data', '', window.location.pathname);			
			ga('send', 'pageview', window.location.pathname.replace(/\/[0-9]+$/, "").replace(/\/$/, "") );	///google analytics
			$("#photoDesc").text("");
			$("#comments .closeBtn").trigger("click");
			
		}).on('locationfound', function(e){
//			console.log("found location");
//			console.log(e);
		}).on('locationerror', function(e){
			console.log("location search was fired, but there was an error");
			console.log(e);
		});
		

		if (window.location.hash.length && window.location.hash.indexOf("pid") === 1)	//hash post link
		{
			if (posts.viewPost(window.location.hash.split("=")[1], obj.mapId, true, obj.jumpToPost))
				ga('send', 'pageview', window.location.pathname.replace(/\/[0-9]+$/, "").replace(/\/$/, "")+"#pid="+window.location.hash.split("=")[1] );	///google analytics
			else
			{
				obj.map.setView(new L.LatLng(startLat, startLng), startZoom);
				ga('send', 'pageview');				
			}
		}
		//else if (obj.locate)
		//	obj.map.locate({setView:true, maxZoom:12});		
		else if (typeof pId !== 'undefined')	///direct link to post
		{
			posts.viewPost(pId, obj.mapId, true, obj.jumpToPost);
			ga('send', 'pageview');
		}
		else
		{
			obj.map.setView(new L.LatLng(startLat, startLng), startZoom);
			if (window.location.hash.length)
			{
				$(window.location.hash+"Btn").trigger('click');
			}
			ga('send', 'pageview');
		}
		
		$("#mapMenuBtn").on("click", function(){
			$(".managementBtn:not(#manage)").removeClass("active");			
			$(".rightSlideIn:not(#mapInfo)").removeClass("open");
			$("#mapInfo").toggleClass("open");	
		})

		$(document).on("click", ".iconBtn.subscribe:not(.static)", function(){
			$this = $(".submap_"+$(this).attr("data-id"));
			$.post("/maps/subscribe", {mapId:obj.mapId}, function(data){
				if (data.status === "suc")
				{
					$this.toggleClass("active");
					if ($this.hasClass("active"))
						$this.text("Subscribed");
					else
						$this.text("Subscribe");
				}
			},"json");
			
		}).on("mouseover", ".iconBtn.subscribe:not(.static)", function(){
			if ($(this).hasClass("active"))
				$(this).text("Unsubscribe");
			else
				$(this).text("Subscribe");			
		}).on("mouseout", ".iconBtn.subscribe:not(.static)", function(){
			if ($(this).hasClass("active"))
				$(this).text("Subscribed");
			else
				$(this).text("Subscribe");			
		})	
	}

	snapmapObj.prototype.setOrder = function(_o)
	{
		this.order = _o;
	}

	snapmapObj.prototype.getCurLatLng = function()
	{
		var obj = this;
		return new L.LatLng(obj.cla, obj.clo);
	}


	snapmapObj.prototype.scanMap = function(_map, _async)
	{
		//console.log("scan map");
		var obj = this;
		if (_async === undefined)
			_async = false;
		/*for (var x = 0; x < myMarkers.markers.length; x+=1)   /////reenable this to prevent new search during image zoom
		{
			if (myMarkers.markers[x].open)
				return false;
		}*/
		obj.scannable = false;
		obj.bounds = _map.getBounds();

		$.ajax({
			'url':'/maps/getPosts',
			'async':_async,
			'type':'post',
			'dataType':'json',
			'data':{
				'latS':obj.bounds.getSouth(),
				'longS':obj.bounds.getEast(),
				'latN':obj.bounds.getNorth(),
				'longN':obj.bounds.getWest(),
				'mapId':obj.mapId,
				'order':obj.order
			},
			'success':function (data){
				if (data.status == 'suc')
					obj.placeMarkers(data.data, _map);
			},
			'error':function(data, x, y){
				console.log(x);
			}
		});

		/*'latS':obj.bounds.getSouthEast().lat,
		'longS':obj.bounds.getSouthEast().lng,
		'latN':obj.bounds.getNorthWest().lat,
		'longN':obj.bounds.getNorthWest().lng,*/
	}

	snapmapObj.prototype.placeMarkers = function(_markers, _map){
		var obj = this;

		obj.clusters.RemoveMarkers();
		obj.marker.length=0;
		obj.bounds = _map.getBounds();

		$('a').remove('.bImg');




		newMarkers: for (var x = 0; x < _markers.length; x+=1)
		{
			var _marker = {
				"init": false,
				"markerLocation": {
					"lat":parseFloat(_markers[x].lat), "long":parseFloat(_markers[x].lng)
				},
				"user": _markers[x].userName,
				"rank": _markers[x].rank,
				"img": _markers[x].image+"",
				"imgId": _markers[x].id,
				"title": _markers[x].title,
				"other": "none",
				"from":"canada",
				"id": "marker"+x,
				"open":false
			};

			/*///looks for duplicate marker, if exsists, moves to next marker
			for(var y = 0, z = obj.marker.length; y < z; y++)
			{
				if (obj.marker[y].data.imgId == _marker.imgId)
					continue newMarkers;
			}*/

			var markerLocation = new L.LatLng(_marker.markerLocation.lat, _marker.markerLocation.long);


			if (obj.bounds.contains(markerLocation)/* && obj.marker.length < 10*/)
			{
				///looks for duplicate marker, if exsists, moves to next marker
				oldMarkers: for(var y = 0, z = obj.marker.length; y < z; y++)
				{
					if (obj.marker[y].data.imgId == _marker.imgId)
						continue newMarkers;
				}

				$('<a class="bImg" id="'+_marker.id+'" href="#pid='+_marker.imgId+'"></a>')
					.appendTo("div#links");


				var rendered = Mustache.render(obj.markerTemplate, {
					markerId:_marker.id,
					patternId:"pattern"+_marker.id,
					imageUrl:_marker.img+".jpg",
				});


				var MyIcon = L.divIcon({		
					iconSize: new L.Point(102, 128),
					iconAnchor: new L.Point(51, 128),
					className:"customMarkOut photoMarker",
					html:rendered
				});
				_marker.icon = MyIcon;
				//create marker using cluster plugin
				obj.marker.push(new PruneCluster.Marker(_marker.markerLocation.lat, _marker.markerLocation.long, _marker));

				obj.marker[x].weight = 1;
				obj.marker[x].category = "photo";				
				obj.marker[x].filtered = false;	//if true, hides marker from results

				obj.clusters.RegisterMarker(obj.marker[obj.marker.length-1]);

			}
		}


// 		for(var y = 0, z = obj.marker.length; y < z; y++)
// 		{

// 			if (obj.bounds.contains(obj.marker)/* && obj.marker.length < 10*/)

			
// 			if (obj.marker[y].data.imgId == _marker.imgId)
// 			{
// 				obj.clusters.RemoveMarker(obj.marker[y]);
// 				obj.marker.splice(y, 1);						
// 			}
// 		}
	}

	snapmapObj.prototype.openPost = function(e, _marker){

		var obj = this;

		if(posts.activePost != null)
			obj.hoverOutImage(e, posts.activePost, true);

		_marker.open = true;
		posts.setActive(_marker);
		e.target.setZIndexOffset(10000);
		history.replaceState('data', '', window.location.pathname.replace(/\/[0-9]+$/, "").replace(/\/$/, "")+"#pid="+_marker.imgId);
		ga('send', 'pageview', window.location.pathname.replace(/\/[0-9]+$/, "").replace(/\/$/, "")+"#pid="+_marker.imgId );	///google analytics

		posts.viewPost(posts.activePost.imgId, obj.mapId, true);

	};

	snapmapObj.prototype.hoverImage = function(e, _marker)
	{
		var obj = this;
		e.target.setZIndexOffset(10000);

		/*obj.mrkImg = $('#'+_marker.id);

		//obj.mrkImg.style();

		var h = obj.mrkImg.height();
		w = obj.mrkImg.width();

		//console.log(e.target.options.zIndexOffset);*/

		/*document.getElementById(_marker.id+"ani1").beginElement();

		var svg = document.getElementById(_marker.id);
		svg.setAttribute('width', '149px');
		svg.setAttribute('height', '145px');
		svg.setAttribute('viewBox', '0 0 149 145');*/


		
		obj.mouseOver = true;
	}

	snapmapObj.prototype.hoverOutImage = function(e, _marker, _mouseOver)
	{
		if (_marker === null)
			return false;


			
		var obj = this;
		obj.mouseOver = _mouseOver;
		if (_marker.open == false || e.type == "click" || e.type == "mouseover")
		{

		}
		if (_marker.open == true && (e.type == "click" || e.type == "mouseover"))
		{
			_marker.open = false;
		}

		/*document.getElementById(_marker.id+"ani6").beginElement();

		var svg = document.getElementById(_marker.id);
		svg.setAttribute('width', '102px');
		svg.setAttribute('height', '127.561px');
		svg.setAttribute('viewBox', '0 0 102 127.561');*/

		if (e.target.setZIndexOffset !== undefined)
			e.target.setZIndexOffset(null);

		//console.log(e.target.options.zIndexOffset);

	}


	snapmapObj.prototype.moveMap = function(_bounds)
	{
		var obj = this;
		//obj.map.setView(new L.LatLng(_lat,_lng), 15);
		//    obj.map.fitWorld();
		//obj.map.zoomOut();
		//
		obj.map.fitBounds(_bounds);
		//obj.map.panInsideBounds(_bounds);
		obj.scanMap(obj.map);
	}

	snapmapObj.prototype.jumpToPost = function(_id, _lat, _lng)
	{
		var obj = this;
		if (!_lat || !_lng)
			return false;
		obj.scannable = false;
		obj.map.setView(new L.LatLng(_lat, _lng), 15);
		obj.scanMap(obj.map);

	}