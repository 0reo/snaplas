var snapmap = null;
var usermanagement = null;
var posts = null;

var winHeight = 0;
var mapHeight = 0;
var $map;
var mapOffset = 35;
var mapbar = 19;
var up;
var activePopup = '';

$(window).load(function() {

	up = new uploadObj();//either expand the upload class or intigrate it into something else
	
	// Hide the address bar!
	setTimeout(function(){
		window.scrollTo(0, 1);
	}, 0);

	//makes map object, but doesn't initialize it yet
	posts = new postsObj();
	snapmap = new snapmapObj();
	usermanagement = new userManagementObj();
	
	$map = $("#map");

	//keeps map correct size when resizing window
	winHeight = $(window).height();
	mapHeight = winHeight - mapOffset - mapbar;

	$map.height(mapHeight+"px");

	$(window).resize(function()
	{
		winHeight = $(window).height();
		mapHeight = winHeight - mapOffset - mapbar;
		$map.height(mapHeight+"px");
		snapmap.map.invalidateSize(false);
		//$("#comments").css({bottom:"-"+$("#comments").height()+"px"}).removeClass("open");		
	});


	//main site event handlers
	if ($("#profile").length)
		snapmap.loggedin = true;

	//close things
	$(document).on("click", ".closeBtn", function(){
		$(this).parents(".popup").fadeOut(200);//close popups
		activePopup = '';
		window.location.hash = '';
		
		//close side section
		$section = $(this).parents("section");
		if ($section.hasClass("rightSlideIn"))
		{
			$(".managementBtn:not(#manage)").removeClass("active");			
		}
		$section.removeClass("open");
	})

	$("#login").on("click", function(){
		$("#profileBox").load("/login").toggle('100');
	})


	$("#profileBtn").on("click", function(){
		usermanagement.profile = {};		
		if ($("#profileBox").html().length)
			$("#profileBox").toggle('100', function(){
				$(this).html("");
			});
		else
		{
			$("#profileBox").load("/login").toggle('100').promise().done(function(){
				usermanagement.handleErrors();	
			});
			
		}
	})


	$(document).on("click", ".aboutBtn", function(){
		if (activePopup == "about")
			activePopup = '';
		else
			activePopup = "about";

		$.get("/about", function(data){
			$(".popup").fadeOut(200).promise().done(function(){
				if (activePopup == "about")
					$("#info").html(data).fadeIn(200);
			});
			
		});
	}).on("click", ".privacyBtn", function(){
			if (activePopup == "privacy")
				activePopup = '';
			else
				activePopup = "privacy";

		$.get("/privacy", function(data){
			$(".popup").fadeOut(200).promise().done(function(){
				if (activePopup == "privacy")
					$("#info").html(data).fadeIn(200);
			});
		});
	}).on("click", ".termsBtn", function(e){
		if (activePopup == "terms")
			activePopup = '';
		else
			activePopup = "terms";

		$.get("/terms", function(data){
			$(".popup").fadeOut(200).promise().done(function(){
				if (activePopup == "terms")
					$("#info").html(data).fadeIn(200);
			});
		});
	});


	$("#sortMenu").on("mouseover", ".navLink", function(){
		var nav = $(this);
		$("#headerMarker").animate({left:nav.offset().left+(nav.width()/2)}, {duration:400, queue:false}, function(){});
	}).on("click", ".navLink", function(){
		snapmap.setOrder($(this).attr("id"));
		snapmap.scanMap(snapmap.map);
	})


	$('#photoBtn').click(function(){
		snapmap.uploadPhoto = !snapmap.uploadPhoto;
		$("#photoText").slideDown(200).delay(5000).slideUp(200);
	});






	$("#filter, #sortMenu .button").click(function(){
		$("#sortMenu").toggleClass("full");
	});
		//$('#sortMenu li').toggle('slow');

	document.getElementById("searchbar").addEventListener("submit", function(e){
	//$("#searchbar").on("submit", function(e){
		e.preventDefault();
		//e.stopImmediatePropagation();
		var fLat; var fLng;
		var searchTerm = $('#search').val();
		$('#search').val('');
		$.get("/search", function(data){		
			
			$("#searchWindow").html(data);
			geocode(searchTerm, $('#searchWindow #wrapper'));
			$("#searchWindow").toggle("slow");
		});
	}, false);



	/*document.getElementById("photoForm").addEventListener("submit", function(e){
		e.preventDefault();
	}, false);*/


	$('input').keypress(function(e) {
		if(e.which == 13 && $(this).attr('id') == 'search') {
			$(this).blur();
		}
	});

    $("#likes").on("click", function(){
		snapmap.likeImage();
    });


	$(document).on('click', '.share', function(e){

		if ($(this).attr('data-id') !== undefined)	//clicked from side panel
		{
			var shareUrl = location.protocol+"//"+window.location.hostname + "/m/" +$(this).attr('data-mapname')+'/'+$(this).attr('data-id');
			var shareText = $(this).parents(".postPreview")[0].children[0].children[0].children[0].innerHTML.replace(/'/g, "\\'");
			console.log(shareText);
		}
		else if ($(this).parent().attr("id") == "panelInfoWindow")	//clicked from open post
		{
			var shareUrl = $("#postInfoLeft a")[0].href+'/'+$(this).parent().parent()[0].dataset.id;
			var shareText = $("#bottomPanel .panelTop .panelTitle").text();
		}
		else if ($(this).hasClass("shareMap"))	//share map
		{
			///set up shitty atlas hack so /m/ becomes /a/
			var mapName = $(this)[0].dataset.name.split(' ').join('-');
			if (mapName == "All")
				var shareUrl = location.protocol+"//"+window.location.hostname;
			else
			var shareUrl = location.protocol+"//"+window.location.hostname + "/m/" +$(this)[0].dataset.name.split(' ').join('-');
			var shareText = $(this).attr("data-name");
		}
		else
			return;




		var shareStr = '<input type="text" value="'+shareUrl+'">'
			//+'<div class="fb-share-button" data-href="'+shareUrl+'" data-type="button" data-width="100" style="vertical-align:super; margin-right:5px; zoom:1;*display:inline"></div>'
			+'<a href="//www.facebook.com/sharer.php?s=100&p[url]='+shareUrl+'" onclick="window.open(\'//www.facebook.com/sharer.php?s=100&p[url]='+shareUrl+'\', \'facebook\', \'width=670, height=450\'); return false;" class="fb-share-button"></a>'
			+'<a href="//twitter.com/share?text='+shareText+' - &url='+shareUrl+'&via=snaplas"  onclick="window.open(\'//twitter.com/share?text='+shareText+' - &url='+shareUrl+'&via=snaplas\', \'facebook\', \'width=575, height=400\'); return false;" class="twitter-share-button"></a>'
			+'<a href="//plus.google.com/share?url='+shareUrl+'" onclick="window.open(\'//plus.google.com/share?url='+shareUrl+'\', \'google+\', \'width=800, height=400\'); return false;" class="gp-share-button"></a>'
			+'<a href="//www.reddit.com/submit?url='+shareUrl+'" onclick="window.open(\'//www.reddit.com/submit?url='+shareUrl+'\', \'reddit\'); return false;" class="reddit-share-button"></a>';




		//$(".popup").fadeOut(200);
		$("#shareWindow").fadeIn(200);
		$("#shareContent").html(shareStr);

		//FB.XFBML.parse(); 
		//twttr.widgets.load();
	});
	
	$( "#comments" ).resizable({ handles: "n" });

	//actually initialize the map	
	snapmap.init(locate, mapId);
	snapmap.map.invalidateSize(false);
	usermanagement.init(snapmap.loggedin);
});


function linkify(text){
    if (text) {
        text = text.replace(
            /((https?\:\/\/)|(www\.))(\S+)(\w{2,4})(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/gi,
            function(url){
                var full_url = url;
                if (!full_url.match('^https?:\/\/')) {
                    full_url = 'http://' + full_url;
                }
                return '<a href="' + full_url + '" target="_blank">' + url + '</a>';
            }
        );
    }
    return text;
}
