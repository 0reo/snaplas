uploadObj = function(){

    var obj = this;

	$(document).on("change", "#file", function(){
	    obj.makeDataUrl(this);
	});

    $(document).on("click", "#changeMap", function(e){
		$("#selectMap").show();

	}).on("click", ".mapPreview", function(e){
			$("#upload .panelTitle span").text($(this).find(".mapTitle").text());
			$("#photoForm input[name='mapId']").val($(this).attr("data-id"));
			$("#selectMap").hide();
	});

    $(document).on("submit", "#photoForm", function(e){
    	$("input[type=submit]","#photoForm").attr("disabled", "true");
		var formData = new FormData($(e.target)[0]);

		e.preventDefault();

		$.ajax({
			url: '/posts/postToMap',
			data: formData,
			processData: false,
			contentType: false,
			dataType:'json',
			type: 'POST',
			success: function(data){
				if (data.status == "suc")
				{
					$("#upload .closeBtn").trigger('click');
					snapmap.scanMap(snapmap.map);
					usermanagement.getPosts();
				}
				else
				{
					alert(data.msg);
				}
				$("input[type=submit]","#photoForm").attr("disabled", "false");
			}
		});        
    });
}

uploadObj.prototype.uploadFile = function()
{
    
}

uploadObj.prototype.makeDataUrl = function(input) {
	
	if (input.files && input.files[0]) {
		if (!input.files[0].type.match('image/jpeg') &&
			!input.files[0].type.match('image/png') &&
			!input.files[0].type.match('image/gif'))
		{
			alert("image must be .jpg, .png, or .gif format");
			$("#file").val("");
		}
		var reader = new FileReader();
		
		reader.onload = function (e) {
			$('#preview').attr('src', e.target.result);
		}
		
		reader.readAsDataURL(input.files[0]);
	}
}