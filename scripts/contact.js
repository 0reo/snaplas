$(function(){

	$("#eml").on("click", function(){
		$.get("/contact", function(data){
			$("#opacity").show();
			$("#emailForm").show("slow").html(data);
		});
	});

	$(document).on("submit", "#contactForm", function(e){
		e.preventDefault();
		contact = {"name": $("#contactName").val(),
			"email":$("#contactEmail").val(),
			"question":$("#contactQuestion").val()};
		$.post("/sendMail", contact, function(data){
			if (data.status == "success")
			{
				$("#opacity").hide();
				$("#emailForm").hide("slow");
			}
			else if (data.status == "failure")
			{
				$('#message').text(data.msg);
			}
		}, "json");
	})
});
